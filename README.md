# Environment Configuration

## Purpose
This repo is a central location for developers, evaluators and potential exminees to start a environment that is used for StanEval exmaniations.

## Requirements To Create A Environment
1. You will need a internet connection
2. Install [vagrant](https://www.vagrantup.com/docs/installation/)
3. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
4. Install the "vagrant-reload" plugin using the following command:
```
 vagrant plugin install vagrant-reload
 ```
5. Have at least 12GB of RAM. Note: If your machine does not have 12GB of RAM, you can modfiy the "vb.memory" value in the Vagrantfiles to a smaller amount in step 3 under How To Create An Environment.

## How To Create An Environment
1. Start by cloning the [repo](https://gitlab.com/90COS/public/staneval-environment-configuration).
2. Determine what environment you want to start (Basic, Senior Windows or Senior Linux). An example exam has been provided in each.
3. How to start a Virtual Environment.

	**Basic:**
	If you want a Basic environment, change to the Windows directory and run command: *__autostart_driver_vm=false vagrant up__*

	**Senior Windows:**
	If you want a Senrio Windows environment, change to the Windows directory and run command: *__autostart_driver_vm=true vagrant up__*
	
	**Senior Linux:**
	If you want a Senior Linux environment, change to the Linux directory and run command: *__vagrant up__*

	Note: if the memory requirments need to be changed make the changes in the vagrant file here

	Note: The first time you run the "vagrant up" command, the initial box download may take up to 30 minutes on a slow network.

## Common Problems and Fixes
If you recieve "LibreSSL SSL_read: SSL_ERROR_SYSCALL, errno 60" when running the "vagrant up" command, this is a connection issue on your network. 
To get around this, change the line "*__options << "--insecure" if @insecure__*" of the downloader.rb file to: "*__options << "--insecure"__*"
You can find your downloader.rb file in \<Vagrant install folder\>\embedded\gems\2.X.X\gems\vagrant-2.X.X\lib\vagrant\util\downloader.rb

## How to create a base box that will be used to create a production ready box
1. Determine what environment you want to create.
Linux(fedora) for Senior Linux
Windows 10 for Basic and Senior Windows
2. Change to the *_Base_Image/\<environment>/_* folder.
3. Run command: *__vagrant up__*
4. Once the box is done, add it to the production server following the instructions in the corresponding README.md.
