#!/bin/bash

# Immediately bail if an error is encountered
#set -e

export PACKAGE_MANAGER=yum
export CMAKE_VERSION=3.10.2
export GCC_TOOLSET_VERSION=7

echo "Installing cmake.  This may take a while..."
sudo mkdir -p /opt/cmake/
pushd /opt/cmake
sudo curl -s -o cmake-${CMAKE_VERSION}-Linux-x86_64.sh https://cmake.org/files/LatestRelease/cmake-${CMAKE_VERSION}-Linux-x86_64.sh
chmod a+x cmake-${CMAKE_VERSION}-Linux-x86_64.sh
sudo bash ./cmake-${CMAKE_VERSION}-Linux-x86_64.sh --skip-license
sudo ln -s /opt/cmake/bin/* /usr/local/bin
popd

echo "Installing required software packages..."
sudo ${PACKAGE_MANAGER} update -y -q
declare -a software_packages=(
    "nasm"
    "vim"
    "man-pages"
    "centos-release-scl"
    "devtoolset-${GCC_TOOLSET_VERSION}-gcc*"
    "kernel-devel-uname-r == $(uname -r)"
    "strace"
    "gdb"
    "epel-release"
    "python34"
)
for p in "${software_packages[@]}"
do
    echo "Installing $p..."
    sudo ${PACKAGE_MANAGER} install "$p" -y -q
    #print("$p")
done



echo "Configuring software packages..."
sudo ln -s /opt/rh/devtoolset-${GCC_TOOLSET_VERSION}/root/usr/bin/gcc /usr/bin/cc
sudo ln -s /opt/rh/devtoolset-${GCC_TOOLSET_VERSION}/root/usr/bin/gcc /usr/bin/gcc
sudo ln -s /opt/rh/devtoolset-${GCC_TOOLSET_VERSION}/root/usr/bin/g++ /usr/bin/g++
sudo bash -c "echo 'PATH=\$PATH:/usr/local/bin/' >> /etc/bashrc"

sudo unlink /lib/modules/`uname -r`/build
sudo ln -s /usr/src/kernels/`uname -r`/ /lib/modules/`uname -r`/build
