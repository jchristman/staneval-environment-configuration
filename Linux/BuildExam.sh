TARGET=/home/vagrant/Desktop/Exam
mkdir -p $TARGET

  declare -a test_questions=(
      "extern"
      "linux_vr1"
      "linux-userspace-multithreaded2"
      "linux_kernel_pid_translate"
      "linux_userspace_looks_for_file"
      "linux_elf_parsing"
  )

for q in "${test_questions[@]}"
do
  cp -R /vagrant/Exam/$q $TARGET
done

cp /vagrant/Exam/resources/.vimrc .
cp /vagrant/Exam/resources/README.md ./Desktop

# rm -rf /vagrant/Exam/
chown -R vagrant:vagrant /home/vagrant/Desktop

set -e
cd ./Desktop/Exam
(cd linux_vr1 && cmake . && make && rm -rf solution main.cpp Makefile CMake* cmake*)
(cd linux_elf_parsing && cmake . && make)
(cd linux-userspace-multithreaded2 && rm -rf solution && cmake . && make && cd grader && cmake . && make && cp *.so ..)
(cd linux_userspace_looks_for_file && rm -f inih && ln -s ../extern/inih inih && cmake . -DCMAKE_BUILD_TYPE=Release && make && rm main.cpp && rm -rf solution)
(cd linux_kernel_pid_translate && rm -rf solution && make)

echo 0 > /proc/sys/kernel/randomize_va_space