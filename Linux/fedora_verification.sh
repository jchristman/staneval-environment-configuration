#!/bin/bash

# ===================================================================
# This script verifies software packages are installed on fedora VM
# ===================================================================

PACKAGE_MANAGER=dnf
LOG_FILE=/vagrant/provisioning.log


# run_id should already be present here!!
echo "url=$1" &>> $LOG_FILE
echo "host_id=$2" &>> $LOG_FILE

# create array of pkgs to install
declare -a pkgs=(               # this list should mirror what's in fedora_intall_packages.sh
    "chromium"
    "sublime-text"
    "nasm"
    "make"
    "gcc-c++"
    "qt5-devel"
    "gcc"
    "boost-devel"
    "cmake"
    "capstone-devel"
    "glibc-devel.i686"
    "libstdc++-devel.i686"
    "kernel-devel-uname-r == $(uname -r)"
    "vim"
    "man-pages"
    "strace"
    "gdb"
    "hexedit"
    "git"
    "python"
    "python-pip"
    #libraries for 32-bit
    "redhat-lsb-core.i686"
    "glib2.i686"
    "libXext.i686"
    "libXi.i686"
    "libSM.i686"
    "libICE.i686"
    "freetype.i686"
    "fontconfig.i686"
    "dbus-libs.i686"
)

# intialize array for pkgs not installed
declare -a not_installed

# loop through pkgs and check if they are installed; add to not_installed if unable to install
for pkg in "${pkgs[@]}"
do
    if ${PACKAGE_MANAGER} install "$pkg" -y -q; then
        echo "$(date "+%m%d%Y %T") : INSTALLED $pkg" &>> $LOG_FILE
    else
        echo "$(date "+%m%d%Y %T") : UNABLE TO INSTALL $pkg" &>> $LOG_FILE
        not_installed=(${not_installed[@]} "$pkg")
        success=false
    fi
done


#Check if files/executables exist
declare -a files=(
    "/home/vagrant/edb-debugger/build/edb"
    "/home/vagrant/idafree70_linux.run"
)
for file in "${files[@]}"
do
    if [ -e $file ]; then
       echo "$(date "+%m%d%Y %T") : INSTALLED $file" &>> $LOG_FILE
    else 
        echo "$(date "+%m%d%Y %T") : UNABLE TO INSTALL $file" &>> $LOG_FILE
        not_installed=(${not_installed[@]} "$file")
        success=false
    fi
done

# create array of python modules to install
declare -a python_modules=(
    "requests"
)

# loop through modules and verify install; add to not_installed if unable
for module in "${python_modules[@]}"
do
    if pip install "$module"; then
         echo "$(date "+%m%d%Y %T") : INSTALLED $module" &>> $LOG_FILE
    else
        echo "$(date "+%m%d%Y %T") : UNABLE TO INSTALL $module" &>> $LOG_FILE
        not_installed=(${not_installed[@]} "$module")
        success=false
    fi
done

# check contents of not_installed to determine success
if [ ${#not_installed[@]} -eq 0 ]; then
    success=true
    echo "$(date "+%m%d%Y %T") : ALL PACKAGES INSTALLED" &>> $LOG_FILE
else
    success=false
    echo "$(date "+%m%d%Y %T") : PACKAGES UNABLE TO INSTALL: ${not_installed[@]}" &>> $LOG_FILE
fi

# create array of executables that need to created successfully
declare -a test_executables=(
    "/home/vagrant/Desktop/Exam/linux_vr1/linux_vr"
    "/home/vagrant/Desktop/Exam/linux_elf_parsing/runtests"
    "/home/vagrant/Desktop/Exam/linux-userspace-multithreaded2/linux_userspace_multithreaded2"
    "/home/vagrant/Desktop/Exam/linux_userspace_looks_for_file/linux_userspace_looks_for_file"
    "/home/vagrant/Desktop/Exam/linux_kernel_pid_translate/user.c"
)

# loop through executables and verify
for executable in "${test_executables[@]}"
do
    if [ -e $executable ]
    then
         echo "$(date "+%m%d%Y %T") : EXECUTABLE BUILT $executable" &>> $LOG_FILE
         success=true
    else
        echo "$(date "+%m%d%Y %T") : EXECUTABLE NOT BUILT $executable" &>> $LOG_FILE
        not_installed=(${not_installed[@]} "$executable")
        success=false
    fi
done

echo "status=$success" &>> $LOG_FILE
