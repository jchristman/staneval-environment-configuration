#!/bin/bash

# Immediately bail if an error is encountered
#set -e

PACKAGE_MANAGER=dnf
CMAKE_VERSION=3.10.2
INSTALLX=true

echo "Installing cmake.  This may take a while..."
sudo mkdir -p /opt/cmake/
pushd /opt/cmake
sudo curl -s -o cmake-${CMAKE_VERSION}-Linux-x86_64.sh https://cmake.org/files/LatestRelease/cmake-${CMAKE_VERSION}-Linux-x86_64.sh
chmod a+x cmake-${CMAKE_VERSION}-Linux-x86_64.sh
sudo bash ./cmake-${CMAKE_VERSION}-Linux-x86_64.sh --skip-license
sudo ln -s /opt/cmake/bin/* /usr/local/bin
popd

# create dnf package for sublime-text
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/dev/x86_64/sublime-text.repo

echo "Installing required software packages..."
sudo ${PACKAGE_MANAGER} update -y -q
declare -a software_packages=(
    "chromium"
    "sublime-text"
    "nasm"
    "make"
    "gcc-c++"
    "qt5-devel"
    "gcc"
    "boost-devel"
    "cmake"
    "capstone-devel"
    "glibc-devel.i686"
    "libstdc++-devel.i686"
    "kernel-devel-uname-r == $(uname -r)"
    "vim"
    "man-pages"
    "strace"
    "gdb"
    "hexedit"
    "git"
    "python"
    "python-pip"
    #libraries for 32-bit
    "redhat-lsb-core.i686"
    "glib2.i686"
    "libXext.i686"
    "libXi.i686"
    "libSM.i686"
    "libICE.i686"
    "freetype.i686"
    "fontconfig.i686"
    "dbus-libs.i686"
)


for p in "${software_packages[@]}"
do
    echo "Installing $p..."
    sudo ${PACKAGE_MANAGER} install "$p" -y -q
done

if [ "$INSTALLX" = true ] ; then
    sudo ${PACKAGE_MANAGER} group install lxde-desktop-environment -ycat fed	
    sudo ${PACKAGE_MANAGER} install gvim -y
    sudo ${PACKAGE_MANAGER} install qhexedit2 -y
    sudo ${PACKAGE_MANAGER} install ddd -y
    VBoxClient --display
fi

echo "Installing required python modules..."
declare -a python_modules=(
    "requests"
)

for p in "${python_modules[@]}"
do
    echo "Installing $p..."
    sudo pip install "$p"
done


#peda and ./gdbinit creation
git clone https://github.com/longld/peda
echo "source ~/peda/peda.py" > ./gdbinit
echo "set disassembly-flavor intel" >> ./gdbinit
echo "set pagination off" >> ./gdbinit
echo "history save on" >> ./gdbinit
echo "set history filename ~/.gdb_history" >> ./gdbinit
echo "history size 32768" >> ./gdbinit
echo "history expansion on" >> ./gdbinit

#ida pro install
wget https://out7.hex-rays.com/files/idafree70_linux.run
chmod 777 ./idafree70_linux.run
#Output says it installs but I cant find location if it actually installed
#sudo yes yes | ./idafree70_linux.run

#install edb
git clone --recursive https://github.com/eteran/edb-debugger.git
sudo mkdir /home/vagrant/edb-debugger/build && cd /home/vagrant/edb-debugger/build && sudo cmake ..
cd /home/vagrant/edb-debugger/build && sudo make
