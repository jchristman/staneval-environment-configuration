#include <sys/procfs.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include "elf.h"

/**
 * ELF patcher and sample implementation
 * The application is invoked with two parameters, the first being the executable you wish to patch and the second
 * being the output path for the new binary.
 *
 * The application will list all shared objects that are linked against with their offsets into the strings table.
 * The approach taken by this application is to rewrite the DT_DEBUG entry into a DT_NEEDED entry and point it at the
 * existing strings table. This limits the options you have for library naming, but it should still be possible to find
 * one of interest. For example, libm.so.6 can be used to create ibm.so.6 which isn't immediately suspicious. Other
 * applications such as the chromium browser provide a significant number of loaded libraries to potentially hide in.
 *
 * Problems: These libraries need to exist somewhere in the library search path or the applications won't launch anymore
 * or running ldd on it will report being unable to find the library which will be a giveaway to the student.
 *
 * Future Work: The dynamic strings table is usually followed by GNU version entries. It should be possible to overwrite
 * those entries with a new library name as long as the dynamic table size is also updated. Those two sections are not
 * mandatory and should not cause problems if removed.
 */

void scan(char* buf, size_t buf_len);

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        printf("Usage: ./elf <input file path> <output file path>\n");
        return 1;
    }

    FILE* fp = fopen(argv[1], "r");
    if (!fp)
    {
        perror("Error opening file");
        return 1;
    }

    struct stat st;
    stat(argv[1], &st);
    size_t filesize = (size_t)st.st_size;
    char* buf = new char[filesize];

    fread(buf, 1, filesize, fp);
    fclose(fp);

    Elf64_Ehdr* elf = (Elf64_Ehdr*)buf;
    if (elf->e_ident[EI_MAG0] != ELFMAG0 && elf->e_ident[EI_MAG1] != ELFMAG1 && elf->e_ident[EI_MAG2] != ELFMAG2 && elf->e_ident[EI_MAG3] != ELFMAG3)
    {
        printf("Invalid ELF header\n");
        return -1;
    }
    printf("ELF format detected\n");

    printf("Type: ");
    switch (elf->e_type)
    {
        case ET_REL:
            printf("Relocatable object");
            break;
        case ET_EXEC:
            printf("Exectuable");
            break;
        case ET_DYN:
            printf("Shared object");
            break;
        case ET_CORE:
            printf("Core");
            break;
        default:
            printf("Unknown");
    }
    printf("\n");

    printf("Machine: ");
    switch (elf->e_machine)
    {
        case EM_386:
            printf("386");
            break;
        case EM_AMD64:
            printf("x86_64");
            break;
        default:
            printf("Other");
    }
    printf("\n");

    printf("Class: ");
    switch(elf->e_ident[EI_CLASS])
    {
        case ELFCLASS32:
            printf("32-bit");
            break;
        case ELFCLASS64:
            printf("64-bit");
            break;
        case ELFCLASSNONE:
            printf("Unknown");
            break;
        default:
            printf("Invalid class");
    }
    printf("\n");

    if (elf->e_ident[EI_CLASS] != ELFCLASS64)
    {
        printf("Only 64-bit binaries are currently supported\n");
        return 1;
    }
    printf("Valid file\n\n");


    printf("Program Header Offset: %u\n", elf->e_phoff);
    if (elf->e_phoff)
    {
        //Elf32_Phdr* phdr32;
        //Elf64_Phdr* phdr64;
        //void* phdr = nullptr;

        printf("Program Header Entries: %d\n", elf->e_phnum);
    }
    printf("\n");

    printf("Section Header Offset: %u\n", elf->e_shoff);
    if (!elf->e_shoff)
    {
        printf("Could not find section header start\n");
        return 1;
    }

    printf("Section Header Entries: %d\n", elf->e_shnum);
    Elf64_Shdr* shdr = nullptr;

    // Find the strings table
    char* str_table = nullptr;
    if (elf->e_shstrndx)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff);
        str_table = (char*)elf + shdr[elf->e_shstrndx].sh_offset;
        printf("String table: 0x%X\n", str_table);
    }

    //void* sym_table = nullptr;
    Elf64_Dyn* dyn_table = nullptr;
    void* dyn_table_end = nullptr;
    char* dyn_str_table = nullptr;

    for (unsigned int i = 0; i < elf->e_shnum; i++)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff + sizeof(Elf64_Shdr)*i);
        //printf("Type: %d\n", shdr->sh_type);

        if (shdr->sh_type && str_table)
        {
            //printf("Section: %d (%d) %s\n", i, shdr->sh_type, str_table + shdr->sh_name);
            if (shdr->sh_type == SHT_DYNSYM)
            {
                //sym_table = &shdr;
            }

            if (shdr->sh_type == SHT_DYNAMIC)
            {
                dyn_table = (Elf64_Dyn*)((char*)elf + shdr->sh_offset);
                dyn_table_end = (char*)dyn_table + shdr->sh_size;
                //printf("dyn_table: %p\tdyn_table_end: %p\tSize: %d\n", dyn_table, dyn_table_end, shdr->sh_size);
            }

            if (shdr->sh_type == SHT_STRTAB)
            {
                // Find the dynamic section's string table
                // It can be found from the dynamic table itself, but that entry stores a process image address
                // This entry provides both the image address (shdr->sh_addr) and the file offset (shdr->sh_offset)
                if (memcmp(str_table + shdr->sh_name, ".dynstr", 7) == 0)
                {
                    if (shdr->sh_offset)
                    {
                        dyn_str_table = (char*)elf + shdr->sh_offset;
                    }
                }
            }
        }
    }

    if (dyn_table == nullptr || dyn_table_end == nullptr)
    {
        printf("Could not find dynamic section\n");
        return 1;
    }

    Elf64_Dyn* dyn_debug = nullptr;
    std::vector<Elf64_Dyn*> dyn_entries;
    std::vector<Elf64_Dyn*> dyn_entries_suspicious;

    printf("\nDynamic Table\n");
    while (dyn_table < dyn_table_end)
    {
        //printf("Tag:%d\n", dyn_table->d_tag);

        if (dyn_table->d_tag == DT_NEEDED)
        {
            //printf("Needed symbol: %s\n", str_table + dyn_table->d_un.d_val);
            dyn_entries.push_back(dyn_table);
        }

        if (!dyn_str_table && dyn_table->d_tag == DT_STRTAB)
        {
            dyn_str_table = (char*)dyn_table->d_un.d_ptr;
        }

        if (dyn_table->d_tag == DT_DEBUG)
        {
            dyn_debug = dyn_table;
        }

        // See if it's the end of the dynamic table
        if (dyn_table->d_tag == DT_NULL)
        {
            break;
        }

        dyn_table++;
    }

    if (dyn_str_table == nullptr)
    {
        printf("Could not find dynamic string table\n");
        return 1;
    }

    printf("DT_NEEDED Libraries:\n");
    for (int i = 0; i < dyn_entries.size(); i++)
    {
        printf("%s\t[off: %d]\n", dyn_str_table + dyn_entries[i]->d_un.d_val, dyn_entries[i]->d_un.d_val);
    }
    printf("\n");

    if (dyn_debug == nullptr)
    {
        printf("No DT_DEBUG entry available for rewriting\n");
        return 1;
    }

    printf("Enter a string offset from the above list to use in the debug entry.\n");
    printf("Offsets may start in the middle of a string.\n");

    unsigned int max_offset = dyn_entries[dyn_entries.size() - 1]->d_un.d_val +
            (unsigned int)strlen(dyn_str_table + dyn_entries[dyn_entries.size() - 1]->d_un.d_val);
    unsigned int offset = 0;

    while (offset == 0 || offset >= max_offset)
    {
        printf("Offset: ");
        scanf("%u", &offset);

        if (offset == 0)
        {
            printf("Offset cannot be zero.\n");
        }
        else if (offset >= max_offset)
        {
            printf("Offset is too large.\n");
        }
        else
        {
            printf("Inserted library will be: %s\n", dyn_str_table + offset);
        }
    }

    dyn_debug->d_tag = DT_NEEDED;
    dyn_debug->d_un.d_val = offset;

    printf("Writing new binary to disk\n");
    FILE* fp_new = fopen(argv[2], "wb");
    if (!fp_new)
    {
        printf("Error opening output file (%s):\n", argv[1]);
        perror(NULL);
        return 1;
    }
    size_t bytes_written = fwrite(buf, 1, filesize, fp_new);
    if (bytes_written != filesize)
    {
        perror("There was an error while writing the file");
        return 1;
    }
    fclose(fp_new);

    delete[] buf;
    return 0;
}