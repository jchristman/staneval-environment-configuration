## ELF Patcher
This tool allows you to convert a DT_DEBUG entry into a DT_NEEDED entry

## Usage
`./elf <input file path> <output file path>`

Given an ELF file, you are presented with a list of libraries existing in the binary and asked for an offset into their names to create a new name. At the moment, the tool can only use existing names and cannot create entirely new entries.

## Example
```
$ ./elf_patcher elf_patcher elf2
ELF format detected
Type: Exectuable
Machine: x86_64
Class: 64-bit
Valid file

Program Header Offset: 64
Program Header Entries: 9

Section Header Offset: 25968
Section Header Entries: 31
String table: 0xC29369

Dynamic Table
DT_NEEDED Libraries:
libstdc++.so.6	[off: 1]
libm.so.6	[off: 255]
libgcc_s.so.1	[off: 265]
libc.so.6	[off: 294]

Enter a string offset from the above list to use in the debug entry.
Offsets may start in the middle of a string.
Offset: 256
Inserted library will be: ibm.so.6
Writing new binary to disk
```
Using offset 256 (in the middle of libm.so.6) creates an entry called "ibm.so.6".



## References
* The ELF specification: http://www.skyfree.org/linux/references/ELF_Format.pdf



Other possibles resources we could give:
## References:
* http://backtrace.io/blog/elf-shared-library-injection-forensics/
* http://vxheaven.org/lib/vrn00.html
* http://www.skyfree.org/linux/references/ELF_Format.pdf
* http://www.sco.com/developers/gabi/latest/contents.html
* https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/
* https://opensource.apple.com/source/dtrace/dtrace-90/sys/elf.h
* https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
* http://rpm5.org/docs/api/readelf_8h-source.html
* https://opensource.apple.com/source/cctools/cctools-523/file/readelf.c
* https://github.com/file/file/blob/master/src/readelf.h
