#include <stdio.h>
#include <memory.h>
#include <vector>
#include "elf.h"
#include "helpers.h"

/**
 * Sample C implementation of an ELF parser looking for DT_NEEDED entries outside of the original list
 * Program is limited to 64-bit binaries for conciseness since the headers change depending on the bitness of the file
 * @param buf - A buffer containing the ELF file (fread of contents)
 * @param buf_len - Unused in this example, but the student should ideally check to make sure they never go out of bounds
 */

void scan(char* buf, size_t buf_len)
{
    //
    // Students should perform some basic validation of the ELF header
    //
    Elf64_Ehdr* elf = (Elf64_Ehdr*)buf;
    if (elf->e_ident[EI_MAG0] != ELFMAG0 && elf->e_ident[EI_MAG1] != ELFMAG1 &&
            elf->e_ident[EI_MAG2] != ELFMAG2 && elf->e_ident[EI_MAG3] != ELFMAG3)
    {
        printf("Invalid ELF header\n");
        return;
    }

    if (elf->e_ident[EI_CLASS] != ELFCLASS64 && elf->e_machine != EM_AMD64)
    {
        printf("Only 64-bit binaries are currently supported\n");
        return;
    }

    if (!elf->e_shoff)
    {
        printf("Cannot find section header\n");
        return;
    }


    Elf64_Shdr* shdr = nullptr;

    //
    // Find the DYNAMIC section containing all linked libraries.
    //
    char* str_table = nullptr;
    if (elf->e_shstrndx)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff);
        str_table = (char*)elf + shdr[elf->e_shstrndx].sh_offset;
    }

    Elf64_Dyn* dyn_table = nullptr;
    void* dyn_table_end = nullptr;
    char* dyn_str_table = getDynamicStringTable(elf);

    if (dyn_str_table == nullptr)
    {
        printf("Error while retreiving string table");
        return;
    }

    // Walk the sections and find the DYNAMIC section
    for (unsigned int i = 0; i < elf->e_shnum; i++)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff + sizeof(Elf64_Shdr)*i);
        if (shdr->sh_type && shdr->sh_type == SHT_DYNAMIC)
        {
            // There isn't an entry count for these tables, so we store the start and end addresses
            dyn_table = (Elf64_Dyn*)((char*)elf + shdr->sh_offset);
            dyn_table_end = (char*)dyn_table + shdr->sh_size;
        }
    }

    if (dyn_table == nullptr || dyn_table_end == nullptr)
    {
        printf("Could not find dynamic section\n");
        return;
    }

    std::vector<Elf64_Dyn*> dyn_entries;
    std::vector<Elf64_Dyn*> dyn_entries_suspicious;
    bool found_end_needed = false;

    //
    // Walk the dynamic table and find dynamic libraries (DT_NEEDED)
    // These entries will be at the beginning of the section. Legitimate entries are contiguous so we need to find
    // a break. Anything not in the initial list is considered rogue.
    //
    while (dyn_table < dyn_table_end)
    {
        if (dyn_table->d_tag == DT_NEEDED)
        {
            if (!found_end_needed) 
            {
                //printf("Needed symbol: %s\n", dyn_str_table + dyn_table->d_un.d_val);
                dyn_entries.push_back(dyn_table);
            }
            else
            {
                // This entry is outside the list of other entries, flag it
                dyn_entries_suspicious.push_back(dyn_table);
            }
        }
        else if (!found_end_needed)
        {
            found_end_needed = true;
        }

        // The end of the dynamic table is marked by a DT_NULL entry
        if (dyn_table->d_tag == DT_NULL)
        {
            break;
        }

        dyn_table++;
    }

    printf("Normal libraries:\n");
    for (int i = 0; i < dyn_entries.size(); i++)
    {
        printf("%s\n", dyn_str_table + dyn_entries[i]->d_un.d_val);
    }
    printf("\n");

    printf("Suspicious libraries:\n");
    if (dyn_entries_suspicious.size())
    {
        for (int i = 0; i < dyn_entries_suspicious.size(); i++)
        {
            printf("%s\n", dyn_str_table + dyn_entries_suspicious[i]->d_un.d_val);
        }
    }
    else
    {
        printf("None\n");
    }
}