#include <memory.h>
#include "elf.h"
/**
 * Given an ELF header, returns a pointer to the strings table.
 *
 * @param Elf64_Ehdr: A pointer to the beginning of an ELF structure
 *
 * @return char*: Pointer to the strings table or NULL if invalid ELF is given
 */
char* getDynamicStringTable(Elf64_Ehdr* elf)
{
    if (elf->e_ident[EI_MAG0] != ELFMAG0 && elf->e_ident[EI_MAG1] != ELFMAG1 &&
            elf->e_ident[EI_MAG2] != ELFMAG2 && elf->e_ident[EI_MAG3] != ELFMAG3)
    {
        //printf("Invalid ELF header\n");
        return nullptr;
    }

    if (elf->e_ident[EI_CLASS] != ELFCLASS64 && elf->e_machine != EM_AMD64)
    {
        //printf("Only 64-bit binaries are currently supported\n");
        return nullptr;
    }

    if (!elf->e_shoff)
    {
        //printf("Cannot find section header\n");
        return nullptr;
    }

    //
    // Find the main strings table which contains the section names.
    // We use this to find the DYNAMIC section containing all linked libraries.
    // Additionally, we need to get the offset of the dyanmic string table which
    // is stored as the offset of the .dynstr section.
    //
    Elf64_Shdr* shdr = nullptr;
    char* str_table = nullptr;
    if (elf->e_shstrndx)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff);
        str_table = (char*)elf + shdr[elf->e_shstrndx].sh_offset;
    }

    char* dyn_str_table = nullptr;

    for (unsigned int i = 0; i < elf->e_shnum; i++)
    {
        shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff + sizeof(Elf64_Shdr)*i);

        if (shdr->sh_type && str_table)
        {
            if (shdr->sh_type == SHT_STRTAB)
            {
                // There can be more than one strings table, find the one for the dynamic section
                if (memcmp(str_table + shdr->sh_name, ".dynstr", 7) == 0)
                {
                    if (shdr->sh_offset)
                    {
                        dyn_str_table = (char*)elf + shdr->sh_offset;
                    }
                }
            }
        }
    }

    return dyn_str_table;
}