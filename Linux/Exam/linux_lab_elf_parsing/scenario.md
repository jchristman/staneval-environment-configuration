
## Scenario
The AF CERT has requested a tool to assist with performing forensics on Linux
based computers. They are looking for a tool which is capable of detecting
malicious libraries that may have been added to existing binaries.

## Task
1. Use the provided elf.h header file to parse the provided binaries
1. Print a list of libraries that the executables are loading
1. Identify whether each library is likely to be malicious or not

## Useful Tools
* readelf
* ldd

## References
* The ELF specification: http://www.skyfree.org/linux/references/ELF_Format.pdf



Other possibles resources we could give:
## References:
* http://backtrace.io/blog/elf-shared-library-injection-forensics/
* http://vxheaven.org/lib/vrn00.html
* http://www.skyfree.org/linux/references/ELF_Format.pdf
* http://www.sco.com/developers/gabi/latest/contents.html
* https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/
* https://opensource.apple.com/source/dtrace/dtrace-90/sys/elf.h
* https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
* http://rpm5.org/docs/api/readelf_8h-source.html
* https://opensource.apple.com/source/cctools/cctools-523/file/readelf.c
* https://github.com/file/file/blob/master/src/readelf.h
