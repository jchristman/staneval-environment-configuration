//
// Created by mike on 5/9/17.
//

#include "sentences.h"

struct sentence_list *
sentence_list_get(struct sentence_list *what, size_t index)
{
    size_t                  current_index = 0;
    struct sentence_list    *return_value = what;

    if(what == NULL)
    {
        ZF_LOGE("sentence_list_get on NULL sentence list");
        goto cleanup;
    }

    for(current_index = 0; current_index < index; current_index++)
    {
        if(return_value == NULL)
        {
            ZF_LOGE("sentence_list_get index greater than size %u",
                    sentence_list_count(what));
            return_value = NULL;
            goto cleanup;
        }
        return_value = return_value->next;
    }

    cleanup:
    return return_value;
}

ssize_t
sentence_list_count(struct sentence_list *what)
{
    struct sentence_list    *current_sentence   = what;
    ssize_t                 return_value        = 0;

    if(what == NULL)
    {
        return_value = -1;
        goto cleanup;
    }

    while(current_sentence != NULL)
    {
        return_value += 1;

        ZF_LOGD("counting sentence @%p next@%p total=%u", current_sentence,
                current_sentence->next,
                return_value);

        current_sentence = current_sentence->next;
    }

    cleanup:

    return return_value;
}

ssize_t
sentence_list_all_sentence_length(struct sentence_list *what)
{
    struct sentence_list    *current_sentence   = what;
    ssize_t                 return_value        = 0;

    if(what == NULL)
    {
        return_value = -1;
        goto cleanup;
    }

    while(current_sentence != NULL)
    {
        return_value += current_sentence->length;

        current_sentence = current_sentence->next;
    }


    cleanup:

    return return_value;
}

enum return_value
sentence_list_populate(struct sentence_list *destination,
                         FILE *source)
{
    ZF_LOGD("sentence_list_populate(destination=%p, source=%p)",
            destination, source);
    char*                   current_line            = NULL;
    enum return_value       return_value            = RETURN_SUCCESS;
    size_t                  current_line_size       = 0;
    size_t                  current_sentence_count  = 0;
    struct sentence_list    *current_sentence       = destination;

    while((getline(&current_line, &current_line_size, source)) != -1)
    {
        current_line_size = strnlen(current_line, current_line_size) - 1;
        if(current_line_size <= 1)
            continue;

        current_line = realloc(current_line, current_line_size + 1);
        current_line[current_line_size] = 0; // null terminator


        ZF_LOGD("ingesting line '%s' len=%d into sentence @%p next @%p",
                current_line, current_line_size, current_sentence,
                current_sentence->next);

        if(current_sentence->next == NULL)
            current_sentence->next = calloc(sizeof(struct sentence_list), 1);

        if(current_sentence->next == NULL)
        {
            ZF_LOGE("could not allocate next sentence structure: %s",
                    strerror(errno));
            return_value = RETURN_OS_ERROR;
            goto cleanup;
        }


        //Subtract 1 from all of these to remove line feed
        current_sentence->length = current_line_size;
        current_sentence->sentence = current_line;
        current_sentence = current_sentence->next;

        current_line = NULL;
        current_line_size = 0;

        current_sentence_count += 1;
    }
    free(current_sentence->next);
    current_sentence->next = NULL;

    cleanup:
    if(current_line != NULL){ free(current_line); }

    return return_value;
}

enum return_value
sentence_list_dealloc(struct sentence_list *to_dealloc)
{
    enum return_value return_value = RETURN_SUCCESS;

    ZF_LOGD("sentence_list_dealloc(to_dealloc=%p(%s))", to_dealloc,
            to_dealloc->sentence);

    if(to_dealloc->next != NULL)
    {
        return_value = sentence_list_dealloc(to_dealloc->next);
    }

    free(to_dealloc->sentence);
    free(to_dealloc);

    return return_value;
}
