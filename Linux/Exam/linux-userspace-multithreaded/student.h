//
// Created by mike on 5/9/17.
//

#ifndef LINUX_USERSPACE_MULTITHREADED_STUDENT_H
#define LINUX_USERSPACE_MULTITHREADED_STUDENT_H

#include <pthread.h>

#include "zf_log.h"

#include "grading.h"
#include "sentences.h"

#endif //LINUX_USERSPACE_MULTITHREADED_STUDENT_H
