//
// Created by mike on 5/7/17.
//

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "zf_log/zf_log.h"

#include "grading.h"
#include "sentences.h"

void
complete_grading(void)
{

}

enum exit_value
start_grading(FILE *source_file)
{
    enum    exit_value return_value             = EXIT_VALUE_NULL;
    enum    student_return_value student_return = STUDENT_INVALID_RETURN_VALUE;
    struct  sentence_list sentences             = {0};
    void    *student_output_buffer              = NULL;
    ssize_t calculated_total_sentence_length    = 0;
    size_t  output_buffer_size                  = 0;

    ZF_LOGD("start_grading(source_file=%p", source_file);
    ZF_LOGI("starting grading");

    if(sentence_list_populate(&sentences, source_file) != RETURN_SUCCESS)
    {
        return_value = EXIT_VALUE_INVALID_ARGUMENTS;
        goto cleanup;
    }

    calculated_total_sentence_length =
            sentence_list_all_sentence_length(&sentences);
    if(calculated_total_sentence_length < 0)
    {
        ZF_LOGE("could not calculate sentence length");
        goto cleanup;
    }
    output_buffer_size = (size_t) calculated_total_sentence_length;

    student_output_buffer = malloc(output_buffer_size);
    if(student_output_buffer == NULL)
    {
        ZF_LOGE("could not allocated student output buffer: %s",
                strerror(errno));
        goto cleanup;
    }

    student_return = student_function(&sentences, student_output_buffer,
                                      output_buffer_size);

    cleanup:
    if(sentences.next != NULL){ sentence_list_dealloc(sentences.next); }
    if(student_output_buffer != NULL){ free(student_output_buffer); };

    return return_value;
}
