#include <argp.h>
#include <string.h>

#define ZF_LOG_LEVEL ZF_LOG_DEBUG
#include "zf_log/zf_log.h"

#include "grading.h"
#include "sentences.h"

struct arguments
{
    char *args[1];
    char *sentence_source_file;
};

static struct argp_option argument_options[] =
{
    {"source-file", 's', "FILE", 0, "the source file from which to read sentences"},
    { 0 },
};

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
    //ZF_LOGD("parse_opt(key=%d(%c), arg=%s, state=%p)", key, key, arg, state);
    struct arguments *arguments = state->input;

    switch (key)
    {
        case 's':
            arguments->sentence_source_file = arg;
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}
static char args_doc[] = "";
static char doc[] = "Linux Apprentice - multithreaded C application";
static struct argp argument_specification = {
    argument_options, parse_opt, 0, doc
};


int main(int argc, char** argv, char** envp)
{
    ZF_LOGD("main starting with %d arguments", argc);
    enum exit_value set_exit_value;
    error_t argument_parse_status;
    FILE *source_file = NULL;
    struct arguments provided_arguments;
    provided_arguments.sentence_source_file = "-";

    argument_parse_status = argp_parse(
            &argument_specification, argc, argv, 0, 0, &provided_arguments
    );
    if(argument_parse_status != 0)
    {
        fprintf(stderr, "Invalid arguments: %s\n",
                strerror(argument_parse_status));
        set_exit_value = EXIT_VALUE_INVALID_ARGUMENTS;
        goto cleanup;
    }

    ZF_LOGI("Running with source file %s",
            provided_arguments.sentence_source_file);

    source_file = fopen(provided_arguments.sentence_source_file, "r");
    if(source_file == NULL)
    {
        ZF_LOGE("invalid source file %s: %s",
                provided_arguments.sentence_source_file,
                strerror(errno));
        goto cleanup;
    }

    set_exit_value = start_grading(source_file);

    cleanup:
    if(source_file != NULL) { fclose(source_file); }

    return (int) set_exit_value;
}