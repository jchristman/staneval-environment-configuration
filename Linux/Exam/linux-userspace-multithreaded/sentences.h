//
// Created by mike on 5/9/17.
//

#ifndef LINUX_USERSPACE_MULTITHREADED_SENTENCES_H
#define LINUX_USERSPACE_MULTITHREADED_SENTENCES_H

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "zf_log.h"

#include "grading.h"

enum return_value
{
    RETURN_SUCCESS, RETURN_FAIL, RETURN_OS_ERROR
};

extern struct sentence_list *
sentence_list_get(struct sentence_list *what, size_t index);

extern ssize_t
sentence_list_count(struct sentence_list *what);

extern ssize_t
sentence_list_all_sentence_length(struct sentence_list *what);

extern enum return_value
sentence_list_populate(struct sentence_list *destination,
                       FILE *source);

extern enum return_value
sentence_list_dealloc(struct sentence_list *to_dealloc);

#endif //LINUX_USERSPACE_MULTITHREADED_SENTENCES_H
