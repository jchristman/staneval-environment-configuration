//
// Created by mike on 5/7/17.
//

#include <assert.h>
#include "student.h"

struct sentence_writer_arguments
{
    pthread_mutex_t         *output_mutex;
    struct sentence_list    *sentence;
    void                    *output_buffer;
};

void *
sentence_writer(void *argument)
{
    struct sentence_writer_arguments    *arguments      =
            (struct sentence_writer_arguments *)argument;
    void                                *return_value   = NULL;

    ZF_LOGD("sentence_writer(%p)", arguments);

    return return_value;
}

enum student_return_value
student_function(struct sentence_list *sentences, void *output_buffer,
                 size_t output_buffer_size)
{
    enum student_return_value
                            return_value            = STUDENT_UNINITIALIZED;
    int                     pthread_return          = 0;
    pthread_mutex_t         output_buffer_mutex     = PTHREAD_MUTEX_INITIALIZER;
    pthread_t               *running_threads        = NULL;
    size_t                  curr_sentence_idx       = 0;
    size_t                  sentence_count          = 0;
    ssize_t                 sentence_count_raw      = 0;
    struct sentence_list    *current_sentence       = NULL;
    struct sentence_writer_arguments
                            *writer_arguments       = NULL;

    sentence_count_raw = sentence_list_count(sentences);
    if(sentence_count_raw < 0)
    {
        ZF_LOGE("could not get sentences count");
        return_value = STUDENT_UNKNOWN_ERROR;
        goto cleanup;
    }
    sentence_count = (size_t) sentence_count_raw;

    running_threads = calloc(sizeof(pthread_t), sentence_count);
    if(running_threads == NULL)
    {
        ZF_LOGE("could not allocate threads array: %s", strerror(errno));
        goto cleanup;
    }

    writer_arguments = calloc(sizeof(struct sentence_writer_arguments),
                              sentence_count);
    if(writer_arguments == NULL)
    {
        ZF_LOGE("could not allocate writer arguments: %s", strerror(errno));
        goto cleanup;
    }

    for(curr_sentence_idx = 0; curr_sentence_idx < sentence_count;
        curr_sentence_idx++)
    {
        ZF_LOGD("creating thread %u/%u with argument @%p",
                curr_sentence_idx + 1, sentence_count,
                &(writer_arguments[curr_sentence_idx]));

        current_sentence = sentence_list_get(sentences, curr_sentence_idx);
        if(current_sentence == NULL)
        {
            ZF_LOGF("could not retreive sentence index %u", curr_sentence_idx);
            assert(0);
        }

        writer_arguments[curr_sentence_idx].output_mutex = &output_buffer_mutex;
        writer_arguments[curr_sentence_idx].sentence = current_sentence;
        writer_arguments[curr_sentence_idx].output_mutex = output_buffer;

        pthread_return = pthread_create(
                &(running_threads[curr_sentence_idx]),
                NULL,
                &sentence_writer,
                &(writer_arguments[curr_sentence_idx])
        );
        switch(pthread_return)
        {
            case 0: // This is the happy path
                ZF_LOGD("created threadId=%u",
                        running_threads[curr_sentence_idx]);
                break;
            case EAGAIN:
                ZF_LOGE("could not create another thread: %s", strerror(errno));
                return_value = STUDENT_OS_ERROR;
                goto cleanup;
            case EINVAL:
                ZF_LOGF("invalid arguments passed to thread creation");
                assert(0);
                break;
            case EPERM:
                ZF_LOGF("insufficient permissions to create threads");
                assert(0);
                break;
            default: // The saddest of all paths... impossible return values
                ZF_LOGF("impossible return value from pthread_create: %d",
                        pthread_return);
                assert(0);
                break;
        }
    }

    for(curr_sentence_idx = 0; curr_sentence_idx < sentence_count;
            curr_sentence_idx++)
    {
        pthread_return = pthread_join(running_threads[curr_sentence_idx], NULL);
        switch(pthread_return)
        {
            case 0:
                break;
            case ESRCH:
                ZF_LOGF("thread ID %u created elsewhere not found again",
                        running_threads[curr_sentence_idx]);
                break;
            case EDEADLOCK:
                ZF_LOGF("deadlock detected");
                assert(0);
                break;
            case EINVAL:
                ZF_LOGF("invalid arguments passed to thread creation");
                assert(0);
                break;
            default:
                ZF_LOGF("impossible return value from pthread_join: %d",
                        pthread_return);
                assert(0);
                break;
        }
    }


    cleanup:
    if(running_threads != NULL)
    {
        for(curr_sentence_idx = 0; curr_sentence_idx < sentence_count;
                curr_sentence_idx++)
        {
            pthread_return = pthread_cancel(running_threads[curr_sentence_idx]);
            switch(pthread_return)
            {
                case ESRCH:
                    ZF_LOGF("thread ID %u created elsewhere not found again",
                            running_threads[curr_sentence_idx]);
                case 0:
                    break;
                default:
                    ZF_LOGF("impossible return value from pthread_cancel: %d",
                            pthread_return);
                    assert(0);
            }
        }
        free(running_threads);
    }
    if(writer_arguments != NULL){ free(writer_arguments);}
    pthread_mutex_destroy(&output_buffer_mutex);

    return return_value;
}
