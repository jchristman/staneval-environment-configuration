//
// Created by mike on 5/7/17.
//

#ifndef LINUX_USERSPACE_MULTITHREADED_STUDENT_HEADER_H
#define LINUX_USERSPACE_MULTITHREADED_STUDENT_HEADER_H

#include <stdio.h>
#include <stddef.h>

/**
 * Linked list which the `student_function' will be provided
 */
struct sentence_list;
struct sentence_list {
    char *sentence;
    size_t length;
    struct sentence_list *next;
};

/**
 * Return values for the `student function'
 */
enum student_return_value {
    STUDENT_SUCCESS = 0,
    STUDENT_INVALID_ARGUMENT = 1,
    STUDENT_UNINITIALIZED = 2,
    STUDENT_OUTPUT_TOO_SMALL = 3,
    STUDENT_INVALID_RETURN_VALUE = 4,
    STUDENT_UNKNOWN_ERROR = 5,
    STUDENT_OS_ERROR = 6,
};
static const char *student_return_value_names[] = {
    "STUDENT_SUCCESS",
    "STUDENT_INVALID_ARGUMENT",
    "STUDENT_UNINITIALIZED",
    "STUDENT_OUTPUT_TOO_SMALL",
    "STUDENT_INVALID_RETURN_VALUE",
    "STUDENT_UNKNOWN_ERROR",
    "STUDENT_OS_ERROR",
    0,
};

/**
 * When the overall application is compiled and run, the exit value will be set
 * to one of these
 */
enum exit_value {
    EXIT_VALUE_PASS = 0,
    EXIT_VALUE_FAIL = 1,
    EXIT_VALUE_INVALID_ARGUMENTS = 2,
    EXIT_VALUE_NULL = 3,
};

static const char *exit_value_names[] = {
        "Exit Pass",
        "Exit Fail",
        "Exit Invalid Arguments",
        "Exit Null",
        0,
};

/**
 * This function should write the provided sentence to the output buffer without
 * breaking any of the sentences. The function should be implemented by creating
 * one thread per sentence and writing all sentences in the same order as the
 * list provided to the output buffer with a null terminator in between. Before
 * exit, the function should call `complete_grading' in the same thread which
 * `student_function' was called from. The function should finally return one of
 * the `student_return_value' enumerated values.
 *
 * Blacklisted functions:
 *  * sleep (or sleep-like functionality)
 *
 * @param sentences the list of sentences to be written to the output buffer
 * @param output_buffer the target buffer
 * @param output_buffer_size the maximum size of the output buffer
 * @return enum student_return_value
 */
enum student_return_value student_function(struct sentence_list *sentences,
                                           void *output_buffer,
                                           size_t output_buffer_size);

/**
 * Grading function which should be called according to the documentation for
 * `student_function'
 * @return void
 */
void complete_grading(void);

enum exit_value start_grading(FILE *source_file);


#endif //LINUX_USERSPACE_MULTITHREADED_STUDENT_HEADER_H
