
use std::io::{BufReader, BufRead, BufWriter, Write};
use std::fs;
use std::path;

extern crate clap;
use clap::{Arg, App};

extern crate data_encoding;
use data_encoding::hex;

extern crate sodiumoxide;
use sodiumoxide::crypto::hash;

struct ProgramArguments
{
    source_file: path::PathBuf,
    destination_file: path::PathBuf,
}

fn hash_file_lines(source: fs::File, destination: fs::File)
{
    let reader = BufReader::new(source);
    let mut writer = BufWriter::new(destination);

    writer.write(b"password,sha256\r\n").unwrap();

    for line_opt in reader.lines() {
        match line_opt {
            Ok(line) => {
                let sha256_hash = hex::encode(&hash::sha256::hash(line.as_bytes())[..]);
                let output = format!("{},{}\r\n", line, sha256_hash);
                writer.write(output.as_bytes()).unwrap();
            },
            Err(_) => {
                break;
            }
        }
    }
}

fn file_exists(path_name: String) -> Result<(), String>
{
    match fs::canonicalize(&path::PathBuf::from(path_name)) {
        Ok(filepath) => {
            match fs::File::open(filepath){
                Ok(_) => Ok(()),
                Err(does_not_exist) => Err(format!("file '{}' does not exist", does_not_exist))
            }
        },
        Err(not_a_path) => Err(format!("not a filepath: {}", not_a_path))
    }
}

fn get_args() -> Option<ProgramArguments >
{
    let app_arguments = App::new("wordlist hasher")
        .arg(Arg::with_name("source")
            .short("s")
            .long("source-filename")
            .value_name("FILE")
            .takes_value(true)
            .help("file name of the source passwords to hash")
            .required(true)
            .validator(file_exists))
        .arg(Arg::with_name("destination")
            .short("d")
            .long("destination-filename")
            .value_name("FILE")
            .takes_value(true)
            .help("file name to overwrite with the passwords and their hashed value")
            .required(true));
    let cli_args = app_arguments.get_matches();

    let source_filepath = path::Path::new(
        cli_args.value_of("source")
        .unwrap_or("wordlist.txt"));
    let destination_filepath = path::Path::new(
        cli_args.value_of("destination")
        .unwrap_or("wordlist-hashed.txt"));

    println!("hashing {} into {}", source_filepath.to_str().unwrap(),
             destination_filepath.to_str().unwrap());

    Some(ProgramArguments{
        source_file: source_filepath.to_owned(),
        destination_file: destination_filepath.to_owned(),
    })
}

fn main() {
    let args_option = get_args();
    if args_option.is_some()
    {
        let args = args_option.unwrap();
        let source_file = fs::File::open(args.source_file).unwrap();
        let destination_file = fs::File::create(args.destination_file).unwrap();
        hash_file_lines(source_file, destination_file);
    }
}
