# Rust-based Reverse Engineering Challenge

## Student Scenario

You are given an application and need to fake the configuration file and
directory structure it expects. Another developer has taken a cursory look at
the application and compiled a wordlist with hashes which the application may
expect. Since their time was cut short, the other developer was unable to
continue work and the challenge is up to you to solve.

In the application, you will need to make it finish execution with a return
code of 0 and find all configuration file and associates directory structures.

## Grading

The application which is provided to the student is written in such a way that
simply looking through the output of the ```strings``` command will prove to be
arduous. To alleviate the issue, students are provided with a wordlist with
hashes; the application has stored within it (or it generates at runtime) hashes
which the student should lookup in the wordlist for the corresponding password.

This application is designed to work for both Windows and Linux. The Linux
environment is easier to set up, but with a little work, the Windows environment
is also possible.

### Preparation

#### Environment

To compile the application, one needs a rust compile toolchain. The installation
instructions may be found at: https://www.rust-lang.org/en-US/other-installers.html
under "Other ways to install rustup".

Externally, the application links against the system version of ```libsodium```
to provide a number of cryptographic operations. To install this on Fedora:
```commandline
sudo dnf -y install libsodium-devel
```

Once the rust toolchain and external dependencies are installed, run the
following command to compile the challenge application:
```commandline
cargo build --release --bin challenge
```

(optional) Run the following command to compile the hashing application:
```commandline
cargo build --release --bin wordlist-hasher
```

The hashing application is only needed to create a new wordlist. Otherwise, one
is provided under `src-wordlist-hasher/`.

The output binaries may be found under `target/release/`.

#### Wordlist

This section is required to recreate the provided wordlist under
`src-wordlist-hasher/wordlist-with-hashes.txt`.

For the student to engage in the challenge, they need to have the wordlist csv
generated. To generate the wordlist, use the following command from the root
of the project:
```
cargo run --bin wordlist-hasher -- -s src-wordlist-hasher/wordlist.txt -d ./output-wordlist.csv
```

Provide the student with `output-wordlist.csv`.

### Section 1: the initial password

  For the initial password, a student should attempt to run the application and
take note of the line:
```
$ ./challenge --password asdf
error: The following required arguments were not provided:
    --password <password>

USAGE:
    challenge --password <password>

For more information try --help
```

From this, the student should understand that perhaps there is a hash from the
csv file they've been provided stored in the binary. There are two ways of
finding the password.

#### Solution

As the student attempts to solve the problem by pouring through the output
of ```strings```, they should realize there is a ton of stuff which for sure
isn't of value. For instance:
* Anything which has a non-hex number (since the application indicates there is
a stored hexlified hash)
* Anything which is shorter than the shortest hash in the wordlist (64)

Thus, the student can do:
``` 
$ strings challenge | egrep "[0-9a-fA-F]{64}"
72AB994FA2EB426C051EF59CAD617750BFE06D7CF6311285FF79C19C32AFD236
/checkout/src/libcore/fmt/mod.rs0000000000000000000000000000000000000000000000000000000000000000truefalse
 is not a char boundary; it is inside  (bytes ) of `0x00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899
```

Then, looking for the first line from the output in the hashlist:
```
$ grep 72AB994FA2EB426C051EF59CAD617750BFE06D7CF6311285FF79C19C32AFD236 wordlist-with-hashes.txt
1q2w3e4r,72AB994FA2EB426C051EF59CAD617750BFE06D7CF6311285FF79C19C32AFD236
```

Providing the password results in:
```commandline
$ ./challenge --password 1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not fetch secret, could not load nonce for secret box: NONCE environment variable not found or does not match hash 34550715062AF006AC4FAB288DE67ECB44793C3A05C475227241535F6EF7A81B
```

### Section 2: the nonce

At this point, the student should have the correct password, but now needs to
find the nonce. The nonce's plaintext can be found using the same lookup as for
the initial password, but looking for the hash printed to the screen:

```commandline
$  grep 34550715062AF006AC4FAB288DE67ECB44793C3A05C475227241535F6EF7A81B src-wordlist-hasher/wordlist-with-hashes.txt 
michael,34550715062AF006AC4FAB288DE67ECB44793C3A05C475227241535F6EF7A81B
```

#### Solution

As the challenge appliation indicates, the nonce needs to be provided as an
environment variable, thus:

```commandline
$ NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not load configuration, could not find configuration, files not found
```

### Section 3: Configuration

At this point, the student needs to figure out what files the challenge
application is attempting to access. The suggested solution is to use
```strace``` to monitor the syscalls which the application executes. The student
should then create the files and directories which the syscalls and error output
indicates.

#### Solution

First, find the directory which the application is attempting to access:
```commandline
$  NONCE=michael strace ./challenge --password=1q2w3e4r
  execve("./challenge", ["./challenge", "--password=1q2w3e4r"], 0x7fffec311288 /* 64 vars */) = 0
  
  <...snip...>
  
  write(1, "password '1q2w3e4r' verified\n", 29password '1q2w3e4r' verified
  ) = 29
  stat("./awesome_configuration", 0x7ffc86ca13d0) = -1 ENOENT (No such file or directory)
  write(2, "could not load configuration, ", 30could not load configuration, ) = 30
  write(2, "could not find configuration, fi"..., 45could not find configuration, files not found) = 45
  write(2, "\n", 1
  )                       = 1
  sigaltstack({ss_sp=NULL, ss_flags=SS_DISABLE, ss_size=8192}, NULL) = 0
  munmap(0x7f126c307000, 8192)            = 0
  exit_group(1)                           = ?
  +++ exited with 1 +++
```

Create the directory & re-run the application. During the next run, the
application will attempt to access a file under the directory, which also
needs to be created:
```commandline
$ mkdir awesome_configuration
$ NONCE=michael strace ./challenge --password=1q2w3e4r
execve("./challenge", ["./challenge", "--password=1q2w3e4r"], 0x7fffec311288 /* 64 vars */) = 0

<...snip...>

write(1, "password '1q2w3e4r' verified\n", 29password '1q2w3e4r' verified
) = 29
stat("./awesome_configuration", {st_mode=S_IFDIR|0775, st_size=0, ...}) = 0
stat("./awesome_configuration", {st_mode=S_IFDIR|0775, st_size=0, ...}) = 0
stat("awesome_configuration/config.json", 0x7fff51c02d90) = -1 ENOENT (No such file or directory)
write(2, "could not load configuration, ", 30could not load configuration, ) = 30
write(2, "could not find configuration, fi"..., 45could not find configuration, files not found) = 45
write(2, "\n", 1
)                       = 1
sigaltstack({ss_sp=NULL, ss_flags=SS_DISABLE, ss_size=8192}, NULL) = 0
munmap(0x7fab8a970000, 8192)            = 0
exit_group(1)                           = ?
+++ exited with 1 +++

$ echo asdf > awesome_configuration/config.json
$ NONCE=michael strace ./challenge --password=1q2w3e4r
  execve("./challenge", ["./challenge", "--password=1q2w3e4r"], 0x7fffec311288 /* 64 vars */) = 0
  
  <...snip...>
  
write(1, "password '1q2w3e4r' verified\n", 29password '1q2w3e4r' verified
) = 29
stat("./awesome_configuration", {st_mode=S_IFDIR|0775, st_size=22, ...}) = 0
stat("./awesome_configuration", {st_mode=S_IFDIR|0775, st_size=22, ...}) = 0
stat("awesome_configuration/config.json", {st_mode=S_IFREG|0664, st_size=0, ...}) = 0
stat("awesome_configuration/config.json", {st_mode=S_IFREG|0664, st_size=0, ...}) = 0
open("awesome_configuration/config.json", O_RDONLY|O_CLOEXEC) = 3
ioctl(3, FIOCLEX)                       = 0
read(3, "", 8192)                       = 0
close(3)                                = 0
write(2, "could not load configuration, ", 30could not load configuration, ) = 30
write(2, "could not load configuration: co"..., 111could not load configuration: could not deserialize configuration: EOF while parsing a value at line 1 column 0) = 111
write(2, "\n", 1
)                       = 1
sigaltstack({ss_sp=NULL, ss_flags=SS_DISABLE, ss_size=8192}, NULL) = 0
munmap(0x7f8208ff9000, 8192)            = 0
exit_group(1)                           = ?
+++ exited with 1 +++
```

NOTE: it is possible that the student will think that they should create
```awesome_configuration``` as a file rather than a directory. Internally,
since the ```struct stat``` presented by the ```stat``` syscall has an entity
type indicating whether the stat'd file exists AND what sort of special file
type the file is (e.g. directory, unix socket, file), the application doesn't
have to issue separate syscalls for understanding whether the file exists and
whether it is a directory (both are checked in the application code). If the
student gets stuck at this point, it is suggested to prompt the student with
questions regarding the nature of files in Linux and how everything, including
directories, is a file. The application gives no indication as to the difference
between the ```awesome_configuration``` directory being made as a file (which
is incorrect) or whether the application tried to find it, but isn't progressing
deeper into its logic.

At this point, the student should see a new line output from the challenge
application with the indication that now there is a deserialization error for
the configuration file:
```commandline
$ ls -lahR awesome_configuration
awesome_configuration:
total 4.0K
drwxrwxr-x. 1 user user  22 Oct 24 10:23 .
drwxrwxr-x. 1 user user 196 Oct 24 10:29 ..
-rw-rw-r--. 1 user user   5 Oct 24 10:30 config.json
$ NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not load configuration, could not load configuration: could not deserialize json configuration: expected value at line 1 column 1
```

### Section 4: The Config File

At this point in the challenge, the student is now presented with the problem
that the application is attempting to read JSON from the configuration file.
Since JSON is considered a simple language, the student can hand-write the
configuration file using an iterative process.

#### Solution

The student should first attempt to make an empty JSON object in the
configuration file:
```commandline
$ echo '{}' > ./awesome_config/config.json
$ NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not load configuration, could not load configuration: could not deserialize json configuration: missing field `listen_port` at line 1 column 2
```

With a missing field for "listen_port", the student can add that in:
```commandline
$ echo '{"listen_port": "asdf"}' > awesome_config/config.json
$ NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not load configuration, could not load configuration: could not deserialize json configuration: invalid type: string "asdf", expected u16 at line 1 column 21
```

Since it appears that there is an incorrect data type for the key, so a student
should understand that ports are unsigned 16-bit unsigned intgers. The fixed
value is:
```commandline
$ echo '{"listen_port": 2013}' > awesome_config/config.json
$ NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
could not load configuration, could not load configuration: could not deserialize json configuration: missing field `listen_address` at line 1 column 21
```

Continuing the process, the student needs to add a listen_address field:
```commandline
$ echo '{"listen_port": 2013, "listen_address": "127.0.0.1"}' > awesome_config/config.json
$ NONCE=michael ./challenge --password=1q2w3e4r
 NONCE=michael ./challenge --password=1q2w3e4r
using provided password 1q2w3e4r
password '1q2w3e4r' verified
successfully loaded configuration to listen at '127.0.0.1:2013'
```

When the student sees the last success line, they have completed the
assignement.

