
extern crate clap;
extern crate data_encoding;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_json;
extern crate sodiumoxide;

mod cli_args;
mod config_loader;
mod password;
mod secret_box;

fn main() {
    let args = cli_args::get_arguments();
    if password::verify_password(&args.password) {
        println!("password '{}' verified", args.password);
        let secret = match secret_box::get_secret(&args.password) {
            Ok(s) => s,
            Err(e) => {
                eprintln!("could not fetch secret, {}", e);
                std::process::exit(1)
            }
        };

        let configuration = match config_loader::get_configuration(&secret) {
            Ok(conf) => conf,
            Err(e) => {
                eprintln!("could not load configuration, {}", e);
                std::process::exit(1);
            }
        };

        println!("successfully loaded configuration to listen at '{}:{}'",
                 configuration.listen_address, configuration.listen_port);
    }else {
        eprintln!("password '{}' does not match stored hexlified hash in application",
                  args.password);
    }
}
