
use clap;

pub struct ProgramArguments {
    pub password: String,
}

pub fn get_arguments() -> ProgramArguments
{
    let app = clap::App::new("secrets box")
        .arg(clap::Arg::with_name("password")
            .short("-p")
            .long("--password")
            .help("password to unlock the secret in the application's secret")
            .takes_value(true)
            .required(true));
    let matches = app.get_matches();

    ProgramArguments {
        password: String::from(matches.value_of("password").unwrap()),
    }
}
