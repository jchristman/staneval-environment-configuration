
use data_encoding;
use sodiumoxide::crypto::hash;

// The password is 1q2w3e4r, sha256=72AB994FA2EB426C051EF59CAD617750BFE06D7CF6311285FF79C19C32AFD236
// The nulls are provided to clean out the output of `strings', length 64 for SHA256 + 1 byte start
// + 1 byte end = length 68
static PASSWORD_HASH: &'static [u8; 66] = &[
    0u8,
    55u8, 50u8, 65u8, 66u8, 57u8, 57u8, 52u8, 70u8, 65u8, 50u8, 69u8, 66u8, 52u8, 50u8, 54u8, 67u8,
    48u8, 53u8, 49u8, 69u8, 70u8, 53u8, 57u8, 67u8, 65u8, 68u8, 54u8, 49u8, 55u8, 55u8, 53u8, 48u8,
    66u8, 70u8, 69u8, 48u8, 54u8, 68u8, 55u8, 67u8, 70u8, 54u8, 51u8, 49u8, 49u8, 50u8, 56u8, 53u8,
    70u8, 70u8, 55u8, 57u8, 67u8, 49u8, 57u8, 67u8, 51u8, 50u8, 65u8, 70u8, 68u8, 50u8, 51u8, 54u8,
    0u8,
];


pub fn verify_password(password: &String) -> bool {
    println!("using provided password {}", password);
    let mut stripped_pwhash = Vec::new();
    stripped_pwhash.extend(PASSWORD_HASH[1..PASSWORD_HASH.len()-1].iter());
    let static_password_bytes = data_encoding::hex::
        decode(stripped_pwhash.as_slice())
        .unwrap();
    let input_password_hash = hash::sha256::hash(password.as_bytes());

    static_password_bytes.iter().zip(input_password_hash[..].iter())
        .all(|(a,b)|a == b)
}
