
use std::env;

use data_encoding;
use sodiumoxide::crypto::hash;
use sodiumoxide::crypto::secretbox;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305;

//   The nonce is michael. The application verifies that the NONCE provided is just michael, but
// then passes michaelmichaelmichaelmic to libsodium as the nonce for the secret box.
//
// This is the sha256 of michael, 34550715062AF006AC4FAB288DE67ECB44793C3A05C475227241535F6EF7A81B
static SECRET_NONCE_HASH: &'static [u8; 32] = &[ 52u8, 85u8, 7u8, 21u8, 6u8, 42u8, 240u8, 6u8, 172u8,
    79u8, 171u8, 40u8, 141u8, 230u8, 126u8, 203u8, 68u8, 121u8, 60u8, 58u8, 5u8, 196u8, 117u8, 34u8,
    114u8, 65u8, 83u8, 95u8, 110u8, 247u8, 168u8, 27u8];

//    The password for the secret box is 1q2w3e4r on the command line. Under the hood, the
//  application will use the SHA256 hash of the password in order to work with libsodium which
// requires as 32-byte hash.
static SECRET_BOX: &'static [u8; 37] = &[
    255u8, 177u8, 162u8, 240u8, 127u8, 164u8, 251u8, 114u8, 10u8, 46u8, 62u8, 115u8, 117u8,
    42u8, 142u8, 49u8, 136u8, 159u8, 235u8, 2u8, 55u8, 34u8, 122u8, 18u8, 42u8, 80u8, 50u8, 131u8,
    130u8, 72u8, 89u8, 13u8, 46u8, 179u8, 135u8, 170u8, 86u8];

fn get_nonce() -> Result<xsalsa20poly1305::Nonce, String>
{
    let raw_nonce = env::var("NONCE").unwrap_or(String::new());
    let nonce_hash: &[u8] = &hash::sha256::hash(raw_nonce.as_bytes())[..];
    if SECRET_NONCE_HASH.iter().zip(nonce_hash.iter()).all(|(a,b)| a == b) {
        let mut nonce_builder = Vec::from(raw_nonce.as_bytes());
        while xsalsa20poly1305::NONCEBYTES > nonce_builder.len() {
            nonce_builder.extend(raw_nonce.as_bytes())
        }
        nonce_builder.resize(xsalsa20poly1305::NONCEBYTES, 0);

        let nonce = xsalsa20poly1305::Nonce::from_slice(
            nonce_builder.as_slice()
        ).unwrap();
        Ok(nonce)
    } else {
        let hexlified_nonce_hash = data_encoding::hex::encode(SECRET_NONCE_HASH);
        Err(format!("NONCE environment variable not found or does not match hash {}",
            hexlified_nonce_hash))
    }
}


fn make_key(hash: hash::sha256::Digest) -> xsalsa20poly1305::Key
{
    let mut keybuilder: Vec<u8> = Vec::with_capacity(xsalsa20poly1305::KEYBYTES);
    keybuilder.extend(hash[..].iter());
    xsalsa20poly1305::Key::from_slice(keybuilder.as_slice()).unwrap()
}

pub fn get_secret(password: &String) -> Result<String, String>
{
    match get_nonce() {
        Ok(nonce) => {
            let password_hash_raw = hash::sha256::hash(password.as_bytes());
            let key = make_key(password_hash_raw);
            let plaintext_res = secretbox::open(
                SECRET_BOX,
                &nonce,
                &key
            );
            match plaintext_res {
                Ok(plaintext) => Ok(String::from_utf8(plaintext).unwrap()),
                Err(_) => Err(String::from("Unable to unlock secret box"))
            }
        },
        Err(error) => Err(format!("could not load nonce for secret box: {}", error))
    }
}
