
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path;

use serde_json;


#[derive(Serialize, Deserialize, Debug)]
pub struct ProgramConfiguration {
    pub listen_port: u16,
    pub listen_address: String,
}

fn read_configuration(source_path: &path::Path) -> Result<ProgramConfiguration, String>
{
    let source_file = fs::File::open(source_path).unwrap();
    let mut reader = io::BufReader::new(source_file);
    let mut file_contents: Vec<u8> = Vec::new();

    reader.read_to_end(file_contents.as_mut()).unwrap();

    match serde_json::from_slice(file_contents.as_slice()) {
        Ok(config) => Ok(config),
        Err(err) => Err(format!("could not deserialize json configuration: {}", err))
    }
}

pub fn get_configuration(source_pathname: &String) -> Result<ProgramConfiguration, String>
{
    let mut source_path = path::PathBuf::from("./");
    source_path.push(path::PathBuf::from(source_pathname));

    let mut source_config_file = path::PathBuf::from(source_pathname);
    source_config_file.push(path::Path::new("config.json"));

    if source_path.exists() && source_path.is_dir() && source_config_file.exists() &&
        source_config_file.is_file(){
        match read_configuration(source_config_file.as_path()) {
            Ok(conf) => Ok(conf),
            Err(e) => Err(format!("could not load configuration: {}", e))
        }
    }else {
        Err(format!("could not find configuration, files not found"))
    }
}
