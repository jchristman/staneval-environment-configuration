# File Hunter #1

## Task
Make the provided application exit with an exit code of 0. This application
looks for a number of external elements which will result in successful execution.
You are expected to use a mix of dynamic and static analysis techniques to make the
application exit with a return code of 0.

## Useful tools
Some tools that may be helpful...

* strace
* gdb
* strings

