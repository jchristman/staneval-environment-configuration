
#include <cstdlib>
#include <experimental/filesystem>
#include <iostream>

#include "inih/cpp/INIReader.h"

namespace fs = std::experimental::filesystem;

static std::vector<char> environment_var_config_path_xor_key = {
        (char) 0xde, (char) 0xad, (char) 0xbe, (char) 0xef,
};

static std::vector<char> environment_var_config_path = {
        (char) 0x92, // L
        (char) 0xf8, // U
        (char) 0xf2, // L
        (char) 0xa9, // F
        (char) 0x98, // F
        (char) 0xf2, // _
        (char) 0xfd, // C
        (char) 0xa0, // O
        (char) 0x90, // N
        (char) 0xeb, // F
        (char) 0xf7, // I
        (char) 0xa8, // G
        (char) 0x81, // _
        (char) 0xfd, // P
        (char) 0xff, // A
        (char) 0xbb, // T
        (char) 0x96, // H
        (char) 0xad, // \x00
        (char) 0xbe, // \x00
        (char) 0xef, // \x00
};

fs::path
get_config_parent_directory()
{
    const char         *config_path_env_value = nullptr;
    std::stringstream  environment_var_builder;
    std::string        environment_var_key;
    for(size_t path_idx = 0;
        path_idx < environment_var_config_path.size();
        path_idx += environment_var_config_path_xor_key.size())
    {
        for(size_t key_idx = 0;
            key_idx < environment_var_config_path_xor_key.size();
            key_idx++)
        {
            char next_char = environment_var_config_path[path_idx+key_idx] ^
                             environment_var_config_path_xor_key[key_idx];
            if('\0' != next_char)
                environment_var_builder << next_char;
        }
    }
    environment_var_key = environment_var_builder.str();

    config_path_env_value = std::getenv(environment_var_key.c_str());
    if(nullptr == config_path_env_value)
    {
        std::cerr << "environment key not found" << std::endl;
        exit(EXIT_FAILURE);
    }

    return fs::path(std::string(config_path_env_value));
}

fs::path
build_path()
{
    std::string raw_configuration_parent_dir = get_config_parent_directory();

    std::string raw_configuration_dir(1,   'c');
    raw_configuration_dir               += 'o';
    raw_configuration_dir               += 'n';
    raw_configuration_dir               += 'f';
    raw_configuration_dir               += 'i';
    raw_configuration_dir               += 'g';

    std::string raw_configuration_file(1, 'c');
    raw_configuration_file             += 'o';
    raw_configuration_file             += 'n';
    raw_configuration_file             += 'f';
    raw_configuration_file             += 'i';
    raw_configuration_file             += 'g';
    raw_configuration_file             += 'u';
    raw_configuration_file             += 'r';
    raw_configuration_file             += 'a';
    raw_configuration_file             += 't';
    raw_configuration_file             += 'i';
    raw_configuration_file             += 'o';
    raw_configuration_file             += 'n';
    raw_configuration_file             += '.';
    raw_configuration_file             += 'i';
    raw_configuration_file             += 'n';
    raw_configuration_file             += 'i';

    return fs::path(raw_configuration_parent_dir) /
            fs::path(raw_configuration_dir) /
            fs::path(raw_configuration_file);
}

bool
check_ini_file(fs::path configuration_path)
{
    INIReader reader(configuration_path.c_str());

    std::cout << "Opened configuration file at " << configuration_path.string()
              << std::endl;

    if(reader.GetBoolean(std::string("default"), std::string("ShouldPass"),
                         false))
    {
        std::cout << "Successfully found value for \"ShouldPass\" variable set "
                     "to true in \"default\" section of configuration file"
                  << std::endl;
        return true;
    }
    return false;
}

int main() {
    bool        successful_exit     = false;
    fs::path    configuration_path  = build_path();
    if(! fs::exists(configuration_path))
    {
        std::cerr << "configuration path not found!" << std::endl;
    } else {
        successful_exit = check_ini_file(configuration_path);
    }
    return successful_exit ? EXIT_SUCCESS : EXIT_FAILURE;
}