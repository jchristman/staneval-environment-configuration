#include <pthread.h>
#include <stdio.h>

#define NUM_THREADS 10
#define NUM_ITERATIONS 1000

namespace {
    char* threadsafeKey = "def0cee1a";

    typedef struct {
        bool isinc;
        unsigned long target;
        unsigned long(*inc)(unsigned long*);
        unsigned long(*dec)(unsigned long*);
    } DECONFLICT, *PDECONFLICT;

    void* tf(void* params)
    {
        PDECONFLICT f = (PDECONFLICT)params;
        if (nullptr == f || nullptr == f->inc || nullptr == f->dec)
            //throw ERROR_INVALID_PARAMETER;
            return NULL;

        for (auto i = 0; i < NUM_ITERATIONS; ++i) {
            f->isinc ? f->inc(&f->target) : f->dec(&f->target);
        }
    }
}

extern "C"  {
    char* threadSync(unsigned long(*incref)(unsigned long*), unsigned long(*decref)(unsigned long*))
    {
        DECONFLICT df = { 0 };
        pthread_t hThreads[NUM_THREADS] = { 0 };
        df.inc = incref;
        df.dec = decref;
        df.isinc = true;

        for (auto i = 0; i < NUM_THREADS; ++i)
            pthread_create(&hThreads[i], NULL, &tf, (void*)&df);

        for (auto i = 0; i < NUM_THREADS; ++i)
            pthread_join( hThreads[i], NULL);

        // we should not be sitting at 0 after incrementing lots of times.
        if (0 == df.target)
            return NULL;

        df.isinc = false;
        for (auto i = 0; i < NUM_THREADS; ++i)
            pthread_create(&hThreads[i], NULL, &tf, (void*)&df);

        for (auto i = 0; i < NUM_THREADS; ++i)
            pthread_join( hThreads[i], NULL);

        char* retval = (0 == df.target) ? threadsafeKey : NULL;

        return retval;
    }
}