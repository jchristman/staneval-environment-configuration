#include <iostream>
#include <dlfcn.h>


/* Test 1 Function Definitions */
typedef void(*SharedFunc)(unsigned long*);
typedef char*(*TestFunc1)(SharedFunc, SharedFunc);

/**
* Thread Synchronization:
* - You have been asked to implement a mechanism for a multithreaded application to
* manage reference counts for a shared resource. The current requirement is to implement
* two functions (matching the SharedFunc prototype, above): one that will increment the
* contended resource in a thread-safe manner, and one that will decrement the resource in
* a thread-safe manner.
*
* - In order to complete this task, you will need to:
* 1.) Load the LinuxGrader.so
* 2.) Get a pointer to the exported threadSync function (which matches the TestFunc1 definition at
* the top of the file)
* 3.) Call threadSync, providing your increment and decrement functions as arguments (the first
* argument being your increment implementation, and the second being decrement).
*
* - If the test succeeds, you will get back a string containing the key.
* If it fails however, (either because the operations are not altering the resource, or because
* they are not thread safe) you will get back a NULL pointer.
*/
void test1()
{
    /*
    Your code here...
    */

}


/**
* All of the tests in this section require functions that are all provided via exports from
* the LinuxGrader.so (provided by test proctor). In order to successfully accomplish the objectives,
* the SO will need to be loaded, and all relevant functions will need to be located.
*/
int main(int argc, char** argv)
{
    test1();

    return 0;
}
