# Multithreading

## Overview
You have been provided with a shared object (libLinuxGrader.so) which 
implements the `threadSync` function. It takes two function pointers as its
arguments; an increment function and a decrement function. You are to implement
these and call `threadSync` in the `test1()` function in `main.cpp`.

## Tasks
1. Load `libLinuxGrader.so` at runtime. This is not linked at compile time.
1. Obtain a pointer to the exported `threadSync` function.
1. Implement a threadsafe function which increments an unsigned long.
1. Implement a threadsafe function which decrements an unsigned long.
1. Call `threadSync` with your two functions as the arguments.

If the test succeeds, you will get back a string containing the key.
If it fails however, either because the operations are not altering the 
resource, or because they are not thread safe you will get back a NULL pointer.

## Building
Use CMake to generate the makefile, then run the make file.
```
$ cmake .
$ make
```
## Environment
This exercise was built and tested on CentOS 7.4 64-bit and CMake 3.8.2.
