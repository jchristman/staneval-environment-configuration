#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


// Computes the Shannon Entropy of a string
// Adapted from https://github.com/ekg/entropy/blob/master/entropy.cpp
double compute_entropy(char* word, int word_len)
{
    unsigned char counts[256];
    memset(counts, 0, 256);

    for (size_t n = 0; n < word_len; n++)
    {
        counts[word[n]]++;
    }

    double ent = 0;
    double ln2 = log(2);

    for (int i = 0; i < sizeof(counts); i++)
    {
        if (counts[i] > 0)
        {
            double freq = (double)counts[i]/(double)word_len;
            ent += freq * log(freq)/ln2;
        }
    }

    ent = -ent;
    return ent;
}

/**
 * The vulnerable function
 *
 * This has been moved into its own function out of process_file so that it's
 * easier to ovewrite EIP.
 */
double compute_entropy_wrapper(char* word, int word_len)
{
    char temp_storage[20];	// Vuln? Pfft, who has words longer than 20 chars? It's fine
    memset(temp_storage, 0, 20);
    memcpy(temp_storage, word, word_len);

    return compute_entropy(temp_storage, word_len);
}

char* _strnchr(char* haystack, size_t buf_len, int offset, const char needle)
{
    if (0 == buf_len || offset >= buf_len)
    {
        return NULL;
    }

    int i = 0;
    char* pos = haystack + offset;
    while (pos != (haystack + buf_len))
    {
        if (*pos == needle)
        {
            return pos;
        }
        i++;
        pos++;
    }

    return NULL;
}

int count_occurrences(char* haystack, size_t buf_len, char needle)
{
    if (0 == buf_len)
    {
        return 0;
    }

    int count = 0;
    char* pos = haystack;
    while (pos != (haystack + buf_len))
    {
        if (*pos == needle)
        {
            count++;
        }
        pos++;
    }
    return count;
}

char* get_next_word(char* buffer, size_t buf_len, int offset, char** end)
{
    char* start_pos = NULL;
    char* end_pos = NULL;

    if (offset >= buf_len)
    {
        return NULL;
    }
    
    // Find the start of the next word
    if (NULL == (start_pos = _strnchr(buffer, buf_len, offset, ' ')))
    {
        fprintf(stderr, "Didn't find a start\n");
        return NULL;
    }
    // Advance one to the first non-space char
    // This can cause oddities since we're not handling multiple spaces
    start_pos++;

    // Find the end of that word
    // If it's the end of the buffer, we may not find a space, so return the last char in the buffer
    if (NULL == (end_pos = _strnchr(buffer, buf_len, start_pos - buffer, ' ')))
    {
        end_pos = buffer + (buf_len - 1);
    }
    
    *end = end_pos;
    return start_pos;
}

void process_file(FILE* file)
{
    char * buffer = 0;
    long file_size;

    fseek(file, 0, SEEK_END);
    file_size = ftell(file);

    buffer = (char *)malloc(file_size);
    if (!buffer)
    {
        fprintf(stderr, "Failed to allocate %u bytes\n", file_size);
        return;
    }

    fseek(file, 0, SEEK_SET);
    size_t nread = fread(buffer, 1, file_size, file);
    if (nread < file_size)
    {
        fprintf(stderr, "Failed to read %zu bytes\n", nread);
        free(buffer);
        return;
    }

    int sentences = count_occurrences(buffer, file_size, '.');
    printf("Found %d sentences", sentences);

    //
    // Compute entropies
    //
    double sum = 0.0;
    unsigned int count = 0;
    char* word_start = NULL;
    char* word_end = NULL;
    size_t word_offset = 0;
    int word_len = 0;
    while (NULL != (word_start = get_next_word(buffer, file_size, word_offset, &word_end)))
    {
        word_len = word_end - word_start;

        if (0 == word_len)
        {
            word_offset++;
            continue;
        }
        
        sum += compute_entropy_wrapper(word_start, word_len);
        count++;

        word_offset += word_len;

        if(word_end - buffer == file_size - 1)
        {
            break;
        }
    }

    printf(" and %u words.\n", count);

    double average = sum / count;
 
    printf("Average word entropy: %f\n", average);

    //free(buffer);
}

int main(int argc, char **argv) 
{
    if (argc != 2)
    {
        fprintf(stderr, "Not enough arguments. Usage: %s <file>\n", argv[0]);
        return -1;
    }

    FILE *fd = fopen(argv[1], "r");
    if (fd != NULL)
    {
        printf("Computing file statistics...\n");
        process_file(fd);
        //fclose(fd);
    }
    else
    {
        perror("fopen");
        return -1;
    }

    return 0;
}
