# Vulnerability Research #1

## Overview
You have been provided with a binary which computes text document statistics.
There is a vulnerability in the way that it processes files. The file to be 
processed is passed as an argument to the application. A sample document has
been provided.

Example: `./linux_vr words.txt`


## Task 1
Using static and dynamic analysis, in addition to the tools provided, such as 
the wordlist gen tool (to assist with fuzzing), and the sample "good" wordlist,
perform the following:

-   Locate the vulnerable condition
-   Generate malformed input that will lead to arbitrary code execution.

## Task 2
Use the vulnerability found in task 1 to perform the following:

- Generate position independent shellcode that will create an empty file (the 
  name and location can be arbitrary).
- Leverage the exploit to inject and run your code via `linux_vr`, and ensure 
  a file gets created.

## Useful Tools
A few tools have been provided to help you.

- `shellcode.asm`: An ASM template for writing your shellcode
- `asm_to_shellcode.sh`: A shell script which takes the above ASM file, compiles
  it and outputs an executable named `shellcode` as well as a string version of
  the shellcode
- `wordlist_gen.py`: A python script to generate a document to feed the 
  application. Placeholder variables have been provided to place the shellcode
  string from the asm_to_shellcode.sh output, EIP, etc...
- `words.txt`: A sample text document that the application is able to parse

