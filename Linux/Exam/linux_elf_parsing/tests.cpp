#define CATCH_CONFIG_MAIN
#include <sstream>
#include "elf.h"
#include "catch.hpp"
#include "helpers.h"
#include "student.h"

static const char* test_bin = "test_files/elf1";

SCENARIO( "An ELF header can be parsed", "[header]" ) {

    GIVEN( "An ELF header" ) {

        WHEN( "a valid header is provided" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* good_elf = (Elf64_Ehdr*)&buf[0];
            THEN( "success is returned" ) {
                REQUIRE(student::validate_header(good_elf) == student::Error::SUCCESS);
            }
        }
        WHEN( "no header is provider" ) {
            THEN( "an appropriate error is returned" ) {
                REQUIRE(student::validate_header(nullptr) == student::Error::INVALID_ELF);
            }
        }
        WHEN( "a header with a bad magic number is provided" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* bad_elf = (Elf64_Ehdr*)&buf[0];
            bad_elf->e_ident[EI_MAG1] = 'e';
            THEN( "an appropriate error is returned" ) {
                REQUIRE(student::validate_header(bad_elf) == student::Error::INVALID_ELF);
            }
        }
        WHEN( "an unsupported architecture is provided" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            elf->e_ident[EI_CLASS] = ELFCLASS32;
            THEN( "an appropriate error is returned" ) {
                REQUIRE(student::validate_header(elf) == student::Error::UNSUPPORTED_ARCH);
            }
        }
        WHEN( "an unsupported machine type is provided" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            elf->e_machine = EM_MIPS;
            THEN( "an appropriate error is returned" ) {
                REQUIRE(student::validate_header(elf) == student::Error::UNSUPPORTED_ARCH);
            }
        }
        WHEN( "an invalid section header offset is provided" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* bad_elf = (Elf64_Ehdr*)&buf[0];
            bad_elf->e_shoff = 0;
            THEN( "an appropriate error is returned" ) {
                REQUIRE(student::validate_header(bad_elf) == student::Error::INVALID_ELF);
            }
        }
    }
}

SCENARIO ( "Sections are properly located", "[sections]" ) {
    GIVEN( "An ELF binary" ) {
        WHEN( "a section that exists is requested" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            Elf64_Shdr* shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff);
            THEN( "a valid pointer should be returned" ) {
                REQUIRE(student::find_section(elf,
                  SHT_PROGBITS, ".text", nullptr) != nullptr);
                REQUIRE(student::find_section(elf, SHT_STRTAB, ".shstrtab", nullptr) ==
                  (char*)elf + shdr[elf->e_shstrndx].sh_offset);
            }
        }
        WHEN( "a non-empty section that exists is requested" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            Elf64_Shdr* shdr = (Elf64_Shdr*)((char*)elf + elf->e_shoff);
            THEN( "a non-zero number of entries should be reported for that section" ) {
                int count = 0;
                student::find_section(elf, SHT_DYNAMIC, ".dynamic", &count);
                REQUIRE(count > 0);
            }
        }
        WHEN( "a section that does not exist is requested" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            THEN( "nullptr should be returned" ) {
                REQUIRE(student::find_section(elf,
                  SHT_STRTAB, ".nothere", nullptr) == nullptr);
            }
        }
        WHEN( "a section type that does not exist is requested" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            THEN( "nullptr should be returned" ) {
                REQUIRE(student::find_section(elf,
                  SHT_STRTAB, ".text", nullptr) == nullptr);
            }
        }
    }
}

SCENARIO ( "The dynamic string table can be found", "[dynstr]" ) {
    GIVEN( "An ELF binary" ) {
        WHEN( "the dynamic string table is requested" ) {
            auto buf = read_to_buf(test_bin);
            Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];
            THEN( "a valid pointer is returned" ) {
                REQUIRE(student::get_dynstr_table(elf) != nullptr);
            }
        }
    }
}


SCENARIO( "The correct shared objects are returned", "[obects]" ) {
    GIVEN( "An ELF binary" ) {
        WHEN( "a binary with dependencies is provided" ) {
            auto buf = read_to_buf(test_bin);
            THEN( "the correct dependencies are reported" ) {
                auto libs = student::find_deps(buf);
                REQUIRE( std::count(libs.begin(), libs.end(), "libm.so.6") == 1 );
                REQUIRE( std::count(libs.begin(), libs.end(), "libstdc++.so.6") == 1 );
                REQUIRE( std::count(libs.begin(), libs.end(), "libgcc_s.so.1") == 1 );
                REQUIRE( std::count(libs.begin(), libs.end(), "libc.so.6") == 1 );
                REQUIRE( libs.size() == 4 );
            }
        }
    }
}
