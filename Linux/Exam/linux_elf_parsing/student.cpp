#include "elf.h"
#include <vector>
#include <string>
#include <memory.h>

#include "student.h"

namespace student {

/**
 * Scans a 64-bit ELF file and return a list of its required libraries (.so files).
 *
 * @param buf - A vector containing the ELF file
 * @return Vector of names of libraries that the ELF will load
 */
std::vector<std::string> find_deps(std::vector<char> & buf)
{
    std::vector<std::string> libraries;
    Elf64_Ehdr* elf = (Elf64_Ehdr*)&buf[0];

    // Pointer to the Dynamic section's string table
    char* dyn_str_table = get_dynstr_table(elf);
    if (!dyn_str_table)
    {
        return {};
    }

    //
    // TODO: Your code here
    //

    return libraries;
}

/**
 * Validate the elf header, performing basic checks for correctness
 *
 * @param elf - A pointer to a buffer containing ELF file contents
 *
 * @return An appropriate element of the Error enum (see file student.h)
 */
Error validate_header(Elf64_Ehdr* elf)
{
    //
    // TODO: Your code here
    //

    return Error::SUCCESS;
}

/**
 * Locates a section within the ELF file
 *
 * @param elf - A pointer to a buffer containing ELF file contents
 * @param sec_type - The section header type (see elf.h)
 * @param sec_name (optional) - The name of the section
 * @param[out] count (optional) - The number of entries in the section if found
 *
 * @return A pointer to the found section
 */
char* find_section(Elf64_Ehdr* elf, Elf64_Word sec_type, const char* sec_name, int* count)
{
    Elf64_Shdr* shdr = nullptr;
    char* shstr_table = nullptr;  // section header string table
    char* found_section = nullptr;

    if (validate_header(elf) != Error::SUCCESS)
    {
        return nullptr;
    }

    //
    // TODO: Your code here
    //

    return found_section;
}

char* get_dynstr_table(Elf64_Ehdr* elf)
{
    return find_section(elf, SHT_STRTAB, ".dynstr", nullptr);
}

} //namespace
