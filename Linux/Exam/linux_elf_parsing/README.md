## Task
1. Use the provided elf.h header file to parse the provided binaries (i.e. the `elf` binary produced by the build).
```
$ ./elf elf
```
1. Implement the scan() function in student.cpp which returns a list of libraries that the executable is loading. Only the names are needed, not the full file path (e.g. libz.so.1).

## Useful Tools
* readelf
* ldd

## References
* The ELF specification: http://www.skyfree.org/linux/references/ELF_Format.pdf

