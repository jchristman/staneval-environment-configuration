#include <fstream>
#include <vector>

#include <memory.h>
#include "elf.h"

/**
 * Given an ELF header, returns a pointer to the strings table.
 *
 * @param Elf64_Ehdr: A pointer to the beginning of an ELF structure
 *
 * @return char*: Pointer to the strings table or NULL if invalid ELF is given
 */
std::vector<char> read_to_buf(const char* filename)
{
    std::ifstream infile(filename);
    std::vector<char> buf;

    if (infile.fail())
        return buf;

    infile.seekg(0, infile.end);
    size_t len = infile.tellg();
    infile.seekg(0, infile.beg);

    if (len > 0)
    {
        buf.resize(len);
        infile.read(&buf[0], len);
    }

    return buf;
}


