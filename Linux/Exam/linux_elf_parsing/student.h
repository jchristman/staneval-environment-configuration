#include <vector>

namespace student {

typedef enum
{
    SUCCESS,
    UNSUPPORTED_ARCH,
    INVALID_ELF,
    INVALID_SECTION,
    SECTION_NOT_FOUND
} Error;


Error validate_header(Elf64_Ehdr* elf);
char* find_section(Elf64_Ehdr* elf, Elf64_Word section_type, const char* name, int* count);
char* get_dynstr_table(Elf64_Ehdr* elf);
std::vector<std::string> find_deps(std::vector<char> & buf);

}

