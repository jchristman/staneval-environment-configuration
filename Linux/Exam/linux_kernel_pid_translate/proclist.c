#include <linux/module.h>
#include <linux/printk.h>
#include <linux/sched/signal.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include "ioctl.h"

#define DRIVER_AUTHOR ""
#define DRIVER_DESC   "A sample driver"
 
static dev_t first; // Global variable for the first device number
static struct cdev c_dev; // Global variable for the character device structure
static struct class *cl; // Global variable for the device class


static long print_processes(ioctl_data* io_data)
{
    struct task_struct* task;
    int buffer_offset = 0;
    int proc_len = 0;

    char *userbuf = io_data->response.data.processes;

    if (userbuf == NULL)
    {
        return ERR_INVALID_PARAM;
    }

    //
    // Your code here
    //

    return ERR_SUCCESS;    
}

static long ret_proc_name_from_pid(ioctl_data* io_data)
{
    //
    // Your code here
    //
 
    return ERR_NOT_FOUND;
}

static long ret_pid_from_proc_name(ioctl_data* io_data)
{
    //
    // Your code here
    //

    return ERR_NOT_FOUND;
}

static long process_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
    ioctl_data io_data;

    if (copy_from_user(&io_data, (const void __user *)arg, sizeof(ioctl_data)) != 0)
    {
		printk("Copying ioctl data failed");
        return -EINVAL;
    }

    long ret = -EINVAL;

	switch (cmd)
	{
		case IOCTL_GET_ALL_PROCS:
			printk("Handling IOCTL_GET_ALL_PROCS");
			ret = print_processes(&io_data);
			break;
		case IOCTL_GET_NAME:
			printk("Handling IOCTL_GET_NAME");
			ret = ret_proc_name_from_pid(&io_data);
			break;
		case IOCTL_GET_PID:
			printk("Handling IOCTL_GET_PID");
			ret = ret_pid_from_proc_name(&io_data);
			break;
		default:
			printk("Unrecognized IOCTL received");
			break;
	}

    return ret;
}

static struct file_operations pugs_fops =
{
    .owner = THIS_MODULE,
    .unlocked_ioctl = process_ioctl
};

static int __init ex_init(void)
{
    if (alloc_chrdev_region(&first, 0, 1, "proclistreg") < 0)
    {
        return -1;
    }

    if ((cl = class_create(THIS_MODULE, "char")) == NULL)
    {
        unregister_chrdev_region(first, 1);
        return -1;
    }

    if (device_create(cl, NULL, first, NULL, "proclist") == NULL)
    {
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }

    cdev_init(&c_dev, &pugs_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
    {
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }

    return 0;
}

static void __exit ex_fini(void)
{
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
}

module_init(ex_init);
module_exit(ex_fini);


MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
