#!/usr/bin/env python3
import ctypes
import fcntl
import os
import random
import re
import string
import sys
import unittest
from ioctl import _IOR, IoctlData, TASK_COMM_LEN

IOCTL_GET_ALL_PROCS = _IOR(ord('p'), 1, 'P')
IOCTL_GET_PID =	_IOR(ord('p'), 2, 'P')
IOCTL_GET_NAME = _IOR(ord('p'), 3, 'P')

ERR_INVALID_PARAM = 1
ERR_NOT_FOUND = 2
ERR_UNABLE_TO_WRITE = 3


class Process:
    """Class to hold process info"""

    def __init__(self, pid, name):
        self.pid = pid
        self.name = name

    def __repr__(self):
        return '{} [{}]'.format(self.name, self.pid)


def getProcList():
    """Returns a list of all Processes currently running on the system"""

    processes = []
    for pid in os.listdir('/proc'):
        if not pid.isdigit():
            continue

        try:
            with open('/proc/{}/stat'.format(pid)) as f:
                contents = f.read()
                match = re.search('(\d+) \(([^)/]+)\)', contents)
                if match is None or len(match.groups()) != 2:
                    continue
                pid = match.group(1)
                name = match.group(2)

                processes.append(Process(int(pid), name[:TASK_COMM_LEN]))
        except Exception as e:
            print('Error reading statfile (/proc/{}/stat): {}'.format(pid, repr(e)))

    return processes


class IoctlTest(unittest.TestCase):
    """
    Base unit test class to handle device opening and tracking of IOCTL information
    All tests should inherit from this class
    """

    def setUp(self):
        self.deviceHandle = open('/dev/proclist', 'r')
        self.procList = getProcList()
        self.ioctlData = IoctlData()

    def tearDown(self):
        self.deviceHandle.close()


class TestGetAllProcs(IoctlTest):
    """Tests the IOCTL_GET_ALL_PROCS IOCTL which retrieves a list of all process names"""

    def testGetAll(self):
        try:
            ret = fcntl.ioctl(self.deviceHandle, IOCTL_GET_ALL_PROCS, self.ioctlData)
            self.assertEqual(1, ret)

            names = set([proc.name.strip(" ()") for proc in self.procList])
            studentProcs = set([p.strip(" ()") for p in self.ioctlData.response.data.processes.decode('utf-8').strip().split('\n')])

            diff = names - studentProcs
            revDiff = studentProcs - names

            self.assertEqual(0, len(diff), 'Missing the following processes: {}'.format(', '.join(diff)))

            # There are a lot of edge case where the student could get processes we ignored due to failed
            # regex match in getProcList; is it sufficient to just verify that they found at least everthing
            # we were able to get from /proc?
            #self.assertEqual(0, len(revDiff), 'Extra entries found: #{}#'.format(', '.join(revDiff)))
        except OSError as e:
            self.fail('Received error code: {}'.format(abs(e.errno)))

    def testInvalidParamAll(self):
        self.ioctlData.response.data.processes = None
        self.ioctlData.request.data.pid = 0
        try:
            fcntl.ioctl(self.deviceHandle, IOCTL_GET_ALL_PROCS, self.ioctlData)
            self.fail('Returned success when provided invalid parameters.')
        except OSError as e:
            self.assertEqual(ERR_INVALID_PARAM, e.errno)

class TestGetNameByPid(IoctlTest):

    def testGetExistingPid(self):
        randProc = self.procList[random.randint(0, len(self.procList))]

        self.ioctlData.request.data.pid = randProc.pid
        fcntl.ioctl(self.deviceHandle, IOCTL_GET_NAME, self.ioctlData)
        self.assertEqual(randProc.name, self.ioctlData.response.data.processes.decode('utf-8').strip())

    def testNonExistingPid(self):
        self.ioctlData.request.data.pid = 2**32 - 1

        try:
            fcntl.ioctl(self.deviceHandle, IOCTL_GET_NAME, self.ioctlData)
            self.fail('Found process with matching PID ({}) when it doesn\'t exist. Buffer: {}'
                      .format(self.ioctlData.pid, self.ioctlData.buffer.decode('utf-8')))
        except OSError as e:
            self.assertEqual(ERR_NOT_FOUND, e.errno)

    def testInvalidParamPid(self):
        self.ioctlData.request.data.pid = 0
        self.ioctlData.response.data.processes = None
        try:
            fcntl.ioctl(self.deviceHandle, IOCTL_GET_NAME, self.ioctlData)
            self.fail('Returned success when provided invalid parameters.')
        except OSError as e:
            self.assertEqual(ERR_INVALID_PARAM, e.errno)

class TestGetPidByName(IoctlTest):

    def testGetExistingName(self):
        randProc = self.procList[random.randint(0, len(self.procList) - 1)]
        # There could be multiple pids associated with this name, so check
        pids = [p.pid for p in self.procList if p.name == randProc.name]

        self.ioctlData.request.data.procName = bytes(randProc.name.encode("ascii"))
        fcntl.ioctl(self.deviceHandle, IOCTL_GET_PID, self.ioctlData)
        self.assertIn(self.ioctlData.response.data.pid.contents.value, pids)

    def testNonExistingName(self):
        randName = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))

        self.ioctlData.request.data.procName = bytes(randName.encode("ascii"))
        try:
            fcntl.ioctl(self.deviceHandle, IOCTL_GET_PID, self.ioctlData)
            self.fail('Found process with matching name ({}) when it doesn\'t exist. Buffer: {}'
                      .format(randName, self.ioctlData.buffer.decode('utf-8')))
        except OSError as e:
            self.assertEqual(ERR_NOT_FOUND, e.errno)

    def testInvalidParamName(self):
        self.ioctlData.response.data.processes = None
        self.ioctlData.response.data.processes = None
        self.ioctlData.request.data.pid = 0
        try:
            fcntl.ioctl(self.deviceHandle, IOCTL_GET_PID, self.ioctlData)
            self.fail('Returned success when provided invalid parameters.')
        except OSError as e:
            self.assertEqual(ERR_INVALID_PARAM, e.errno)


if __name__ == "__main__":
    if os.geteuid() != 0:
        sys.exit('You must run this script as root.')

    unittest.main()
