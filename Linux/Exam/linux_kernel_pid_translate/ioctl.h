#ifndef APPRENTICE_IOCTL_H
#define APPRENTICE_IOCTL_H
#include <linux/ioctl.h>
#include <linux/sched.h>

#ifndef TASK_COMM_LEN
#define TASK_COMM_LEN 16
#endif
 
typedef struct
{
    union
    {
        pid_t pid;  // For requests of type IOCTL_GET_NAME
        char procName[TASK_COMM_LEN]; // For requests of type IOCTL_GET_PID
    } data;
} ioctl_request;

typedef struct
{
    union
    {
        pid_t *pid; // For responses of type IOCTL_GET_PID
        char *processes; //For responses of type IOCTL_GET_NAME and IOCTL_GET_ALL_PROCS
    } data;
    size_t buffer_size;
} ioctl_response;

typedef struct
{
    ioctl_request  request;
    ioctl_response response;
} ioctl_data;

// IOCTL codes
#define IOCTL_GET_ALL_PROCS _IOR('p', 1, ioctl_data *)
#define IOCTL_GET_PID       _IOR('p', 2, ioctl_data *)
#define IOCTL_GET_NAME      _IOR('p', 3, ioctl_data *)

// Error codes
#define ERR_INVALID_PARAM   -1
#define ERR_NOT_FOUND       -2
#define ERR_UNABLE_TO_WRITE -3
#define ERR_SUCCESS         1
 
#endif
