#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdlib.h>
 
#include "ioctl.h"
 
int main(int argc, char *argv[])
{
    char *file_name = "/dev/proclist";
    int fd;

    fd = open(file_name, O_RDWR);
    if (fd == -1)
    {
        perror("proclist open");
        return 2;
    }
 
    ioctl_data d;
    ioctl_response *r = &d.response;
    r->buffer_size = 8192;
    r->data.processes = (char*)malloc(r->buffer_size);
 
    if (ioctl(fd, IOCTL_GET_ALL_PROCS, &d) == -1)
    {
        perror("Failed during get_proc_all");
    }
    else
    {
        printf("Process list:\n==================\n%s\n==================\n", r->data.processes);
    }

    free(r->data.processes);
 
    close (fd);
 
    return 0;
}
