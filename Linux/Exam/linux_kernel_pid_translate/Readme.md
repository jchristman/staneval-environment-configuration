# Kernel Driver: Process Traversal

A partial linux kernel driver (proclist.c) has been provided which responds to three IOCTLs:
* IOCTL_GET_ALL_PROCS - Returns a list of all running processes
* IOCTL_GET_PID - Given a process name, returns its PID
* IOCTL_GET_NAME - Given a PID, returns its process name

Students are to use kernel techniques to implement the above IOCTLs.

## Building
To build the driver and tester, run: `make`

## Installing
To install the new kernel driver, run: `sudo insmod proclist.ko`

To remove it, run: `sudo rmmod proclist.ko`

## Verification
To verify that the driver is working correctly, run: `sudo ./grade.py`


## Environment
The exercise was built and tested on Fedora release 25, using the 4.13.16-100.fc25.x86_64 kernel.
