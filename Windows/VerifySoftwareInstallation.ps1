﻿$vs2017 = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\Common7\\IDE\\devenv.exe"
$git = "C:\\Program Files\\Git\\cmd\\git.exe"
$chrome = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
$ida = "C:\\Program Files\\IDA Freeware 7.0\\ida64.exe"
$nasm = "C:\\Program Files\\NASM\\nasm.exe"
$notepad = "C:\\Program Files\\notepad++\\notepad++.exe"
$pycharm = "C:\\Program Files (x86)\\JetBrains\\PyCharm Community Edition *\\bin\\pycharm64.exe"
$python2 = "C:\\Python27\\python.exe"
$python3 = "C:\\Python3*\\python.exe"
$sublime = "C:\\Program Files\\Sublime Text 3\\sublime_text.exe"
$sysinternals = "C:\\ProgramData\\chocolatey\\bin\\procmon.exe"
$vcredist = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\VC\\Redist\\MSVC\\1*\\vcredist_x64.exe"
$vscode = "C:\\Program Files\\Microsoft VS Code\\Code.exe"
$windbg = "C:\\Program Files (x86)\\Windows Kits\\10\\Debuggers\\x64\\windbg.exe"
$wireshark = "C:\\Program Files\\Wireshark\\wireshark.exe"

$rtn = 0
$uri = $args[0]
$ip = $args[1]
$logFile = "C:\\vagrant\\provisioning.log"

if(Test-Path ".\provisioning.log")
{
    Remove-Item ".\provisioning.log"   
}

if(Test-Path $vs2017)
{
    Write-Output "Visual Studio 2017 installed" | Add-Content $logfile 
}
else
{
    Write-Output "Visual Studio 2017 not located.., " | Add-Content $logfile 
    Write-Output "Searched for executable: $vs2017 " | Add-Content $logfile 
    $rtn--
}

if(Test-Path $git)
{
    Write-Output "Git installed" | Add-Content $logfile 
}
else
{
    Write-Output "Git not located..,"  | Add-Content $logfile 
    Write-Output "Searched for executable: $git " | Add-Content $logfile 
    $rtn--
}

if(Test-Path $chrome)
{
    Write-Output "Google Chrome installed" | Add-Content $logfile 
}
else
{
    Write-Output "Google Chrome not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $chrome" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $ida)
{
    Write-Output "IDA installed" | Add-Content $logfile 
}
else
{
    Write-Output "IDA not located..,n" | Add-Content $logfile 
    Write-Output "Searched for executable: $ida " | Add-Content $logfile 
    $rtn--
}

if(Test-Path $nasm)
{
    Write-Output "NASM installed " | Add-Content $logfile 
}
else
{
    Write-Output "NASM not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $nasm" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $notepad)
{
    Write-Output "Notepad++ installed" | Add-Content $logfile 
}
else
{
    Write-Output "Notepad++ not located.., " | Add-Content $logfile 
    Write-Output "Searched for executable: $notepad "| Add-Content $logfile  
    $rtn--
}

if(Test-Path $pycharm)
{
    Write-Output "Pycharm installed " | Add-Content $logfile 
}
else
{
    Write-Output "Pycharm not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $pycharm" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $python2)
{
    Write-Output "Python2 installed "| Add-Content $logfile  
}
else
{
    Write-Output "Python2 not located.., "| Add-Content $logfile  
    Write-Output "Searched for executable: $python2 " | Add-Content $logfile 
    $rtn--
}

if(Test-Path $python3)
{
    Write-Output "Python3 installed" | Add-Content $logfile 
}
else
{
    Write-Output "Python3 not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $python3"| Add-Content $logfile  
    $rtn--
}

if(Test-Path $sublime)
{
    Write-Output "Sublime Text installed" | Add-Content $logfile 
}
else
{
    Write-Output "Sublime Text not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $sublime" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $sysinternals)
{
    Write-Output "SysInternals installed" | Add-Content $logfile 
}
else
{
    Write-Output "SysInternals not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $sysinternals"| Add-Content $logfile  
    $rtn--
}

if(Test-Path $vcredist)
{
    Write-Output "VCRedistrutible installed" | Add-Content $logfile 
}
else
{
    Write-Output "VCRedistrutible not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $vcredist" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $vscode)
{
    Write-Output "Visual Studio Code installed" | Add-Content $logfile 
}
else
{
    Write-Output "Visual Studio Code not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $vscode" | Add-Content $logfile 
    $rtn--
}

if(Test-Path $windbg)
{
    Write-Output "WinDBG installed "| Add-Content $logfile   
}
else
{
    Write-Output "WinDBG not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $windbg " | Add-Content $logfile 
    $rtn--
}

if(Test-Path $wireshark)
{
    Write-Output "Wireshark installed" | Add-Content $logfile 
}
else
{
    Write-Output "Wireshark not located..," | Add-Content $logfile 
    Write-Output "Searched for executable: $wireshark "| Add-Content $logfile 
    $rtn-- 
}

iex "python -m pip install requests"
if ($?){
    Write-Output "requests module installed" | Add-Content $logfile
} else {
    Write-Output "Could not install requests module..," | Add-Content $logfile 
    $rtn-- 
}

iex "python -m pip install pefile"
if ($?){
    Write-Output "pefile module installed" | Add-Content $logfile
} else {
    Write-Output "Could not install pefile module..," | Add-Content $logfile 
    $rtn-- 
}

iex "python -m pip install xmlrunner"
if ($?){
    Write-Output "xmlrunner module installed" | Add-Content $logfile
} else {
    Write-Output "Could not install xmlrunner module..," | Add-Content $logfile 
    $rtn-- 
}

$VSLicense = '"C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\Common7\\IDE\\StorePID.exe" KHJ4N-892F7-JJMJT-7GC7P-9QCDH 08860'
iex "& $VSLicense"
if ($?){
    Add-Content $logfile "VS license accepted"
} else {
    $rtn--
}

if ($rtn -eq 0) {
    $status = "true"
} else {
    $status = "false"
}

#Invoke-RestMethod -Uri $uri -Method PUT -Body $logfile
Add-Content $logFile "url=$uri"
Add-Content $logFile "status=$status" 
Add-Content $logFile "host_id=$ip"
return $rtn 
