#Set GTEST_OUTPUT env variable
[System.Environment]::SetEnvironmentVariable('GTEST_OUTPUT', 'xml:unittest.xml', 'Machine')

#Silent errors because im scared of red
$ErrorActionPreference = "SilentlyContinue"

#VS builder
$msBuildExe = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe'

#Find the newest Directory on the Desktop (Exam folder)
$desktop = "C:\\Users\\vagrant\\Desktop\\"
$examFolder = Get-ChildItem -Path $desktop | Where-Object { $_.PSIsContainer } | Sort-Object LastWriteTime -Descending | Select-Object -First 1

#Set path for examinees directory
$examFolder = $desktop + $examFolder.name

#Python command to run python programs
$python = "python" 

#Run C programs
#Perform test of C Programming section of an evaluation
$cprogramming = $examFolder + "\\C_Programming\\" #build folder path
Set-Location $cprogramming
Write-Host $cprogramming
& "$($msBuildExe)" ".\CProgramming.sln" /t:Build /m
$cquestion = $cquestion + "\\Debug\\"
Set-Location $cquestion
iex "& .\*.exe"
"C Programming: `n" + (Get-Content "unittest.*" -Raw) | Set-Content "unittest.*"
$checkpath = Test-Path $cprogramming

####################################################################################################
### This is a temporary solution to handle the evaluation 7 directory structure
####################################################################################################
#verify folder exists
if($checkpath -eq $True)
{
  Set-Location $cprogramming 
  $pquestion_folders = get-childitem -path $cprogramming | where-object {$_.psiscontainer -eq "True"} | select-object name 
  foreach($pquestion_folder in $pquestion_folders) #iterate through each folder
  {
      $cquestion = $cprogramming + $pquestion_folder.Name
      Set-Location $cquestion
      Write-Host $cquestion
      & "$($msBuildExe)" ".\CProgramming.sln" /t:Build /m
      $cquestion = $cquestion + "\\Debug\\"
      Set-Location $cquestion
      iex "& .\*.exe"
      "C Programming: `n" + (Get-Content "unittest.*" -Raw) | Set-Content "unittest.*"
  }
 }
###############################################################################################

#Run assembly program
$assembly = $examFolder + "\\Assembly_Programming\\" #build assembly path yada yada
$checkpath = Test-Path $assembly
if($checkpath -eq $True)   #if assembly folder exists 
{
  Set-Location $assembly
  & "$($msBuildExe)" ".\ASM.sln" /t:Build /m
  $assembly = $assembly + "\\Debug\\"
  Set-Location $assembly
  iex "& .\ASM.exe"
  "Assembly: `n" + (Get-Content "unittest.*" -Raw) | Set-Content "unittest.*"
}

#Run python programs
$pythonfolder = $examFolder + "\\Python_Programming\\"
if((Test-Path $pythonfolder) -eq $True)   
{
  Set-Location $pythonfolder
  $pquestion_folders = get-childitem -path $pythonfolder | where-object {$_.psiscontainer -eq "True"} | select-object name 
  foreach($pquestion_folder in $pquestion_folders) #iterate through each Python question folder
  {
      Set-Location ($pythonfolder + $pquestion_folder.Name)
      $prog = Get-ChildItem 'r*.py'
      iex "& $python $prog" 
      "Python: `n" + (Get-Content "unittest.*" -Raw) | Set-Content "unittest.*"
  }
}

#Run networking program
$networkprog = $examFolder + "\\Network_Programming\\"
if((Test-Path $networkprog) -eq $True)
{
  Set-Location $networkprog
  $prog = Get-ChildItem -Path $networkprog -File -Filter "*.py"
  iex "& $python $prog"
  "Networking: `n" + (Get-Content "unittest.*" -Raw) | Set-Content "unittest.*"
}

#Combine all unittest to 1 file
#Create file to write all unittests
Remove-Item -path ($examFolder + "\\unittestsAll.xml") -force
$unittestsFile = New-Item -Path $examFolder -Name "unittestsAll.xml" -ItemType "file"
Get-ChildItem $examFolder -Include "unittest.*" -Recurse | ForEach-Object {gc $_; ""} | Out-File $unittestsFile
$subject = Split-Path -leaf -path (Get-Location) "$p> "

Set-Location "C:\\vagrant"
