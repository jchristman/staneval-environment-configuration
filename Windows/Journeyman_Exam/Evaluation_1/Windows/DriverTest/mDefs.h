#pragma once
#include <Windows.h>
#include <functional>
#include <string>

#define EVAL_IMGNAME_IOCTL	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define EVAL_EMPTYQUEUE_IOCTL	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)

template <typename T>
auto ntfunc(std::string name) 
{
	return std::function<std::remove_pointer<T>::type>(reinterpret_cast<T>(GetProcAddress(GetModuleHandleA("ntdll"), name.c_str())));
}

typedef struct _UNICODE_STRING {
	USHORT Length;
	USHORT MaximumLength;
	PWCHAR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;


using ZwLoadDriver_t = NTSTATUS(NTAPI *)(PUNICODE_STRING);
using RtlInitUnicodeString_t = VOID(NTAPI *)(PUNICODE_STRING, PWCHAR);
