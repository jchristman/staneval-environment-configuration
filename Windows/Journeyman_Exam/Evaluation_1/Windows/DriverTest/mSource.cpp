#include <Windows.h>
#include <ImageHlp.h>
#include <thread>
#include <array>
#include <future>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <algorithm>
#include <vector>
#include <locale>
#include <codecvt>
#include <iostream>
#include "mDefs.h"


#define NT_SUCCESS(x)	(x >= 0)

namespace {
	std::mutex					gConLock;
	CONSOLE_SCREEN_BUFFER_INFO	gConsoleInfo;
	HANDLE						gConHandle;
	WORD						gConAttribs;
	
	constexpr UCHAR	rand_in_range(UCHAR val)
	{

		return val;
	}

	int serviceSet(std::string drvPath, std::string svcName);
	int serviceDel(std::string svcName);
	void initializeAttributes();
	void resetAttributes();
	void printError(std::string);
	void printSuccess(std::string);
	void runStuff(DWORD);
	void queryStuff(DWORD);
	void lastTest();
	std::vector<std::wstring> importList(std::wstring);
}


int main(int argc, char** argv, char** envp)
{
	int status = ERROR_SUCCESS;
	NTSTATUS s = 1;
	UNICODE_STRING  buf = { 0 };
	std::vector<std::thread> threads;

	//std::string path = (argc > 1) ? argv[1] : "evalDriver";
	//if (ERROR_SUCCESS != (status = serviceSet(path, "EvalDrv"))) {
	//	return status;
	//}

	try {
		initializeAttributes();
		std::cout << "[*] Beginning sequence..." << std::endl;
		runStuff(800);
		runStuff(800);

		std::cout << "[*] Still working..." << std::endl;
		Sleep(3000);

		queryStuff(300);
	
		Sleep(1000);

		lastTest();
		resetAttributes();

	}
	catch (const std::exception& e) {
		std::cout << "[x] Error! Message: " << e.what() << std::endl;
		return -1;
	}
	catch (...) {
		std::cout << "Error! " << std::to_string(GetLastError()) << std::endl;
	}


	std::cout << "[*] Sequence complete!" << std::endl;
	return status;
}



namespace {


	int serviceSet(std::string drvPath, std::string svcName)
	{
		CHAR buf[MAX_PATH + 1] = { 0 };
		STARTUPINFOA startInfo = { 0 };
		PROCESS_INFORMATION procInfo = { 0 };
		int status = ERROR_SUCCESS;

		std::string command = "cmd.exe /c sc create " + svcName + " type= kernel start= demand binPath= " + drvPath + " && sc start " + svcName;
		_snprintf_s(buf, MAX_PATH, "%s", command.c_str());

		startInfo.cb = sizeof(STARTUPINFOA);
		startInfo.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		startInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);

		if (!CreateProcessA(NULL, buf, NULL, NULL, FALSE, 0, NULL, NULL, &startInfo, &procInfo)) {
			status = GetLastError();
			std::cout << "Create process failed with error: " << std::to_string(status) << std::endl;
			return status;
		}

		WaitForSingleObject(procInfo.hThread, INFINITE);

		return status;
	}

	int serviceDel(std::string svcName)
	{
		CHAR buf[MAX_PATH + 1] = { 0 };
		STARTUPINFOA startInfo = { 0 };
		PROCESS_INFORMATION procInfo = { 0 };
		int status = ERROR_SUCCESS;

		std::string command = "cmd.exe /c sc delete " + svcName;
		_snprintf_s(buf, MAX_PATH, "%s", command.c_str());

		startInfo.cb = sizeof(STARTUPINFOA);
		startInfo.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		startInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);

		if (!CreateProcessA(NULL, buf, NULL, NULL, FALSE, 0, NULL, NULL, &startInfo, &procInfo)) {
			status = GetLastError();
			std::cout << "Create process failed with error: " << std::to_string(status) << std::endl;
			return status;
		}

		WaitForSingleObject(procInfo.hThread, INFINITE);

		return status;
	}


	void initializeAttributes()
	{
		std::unique_lock<std::mutex> lock(gConLock);

		if (INVALID_HANDLE_VALUE == (gConHandle = GetStdHandle(STD_OUTPUT_HANDLE))) {
			throw std::exception(("Unable to get STDOUT; error: " + std::to_string(GetLastError())).c_str());
		}

		if (!GetConsoleScreenBufferInfo(gConHandle, &gConsoleInfo)) {
			throw std::exception(("Unable to get console screen info; error: " + std::to_string(GetLastError())).c_str());
		}

		gConAttribs = gConsoleInfo.wAttributes;

	}


	void resetAttributes()
	{
		std::unique_lock<std::mutex> lock(gConLock);
		if(!SetConsoleTextAttribute(gConHandle, gConAttribs))
			throw std::exception(("Unable to set console attributes due to error: " + std::to_string(GetLastError())).c_str());

	}

	std::vector<std::wstring> importList(std::wstring imgPath)
	{
		std::vector<std::wstring> imports;
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> convrt;
		auto tmp = LoadLibraryExW(imgPath.c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE);
		if (NULL == tmp)
			throw std::exception(("Unable to load requested library! Error message: " + std::to_string(GetLastError())).c_str());

		auto nthdr = (PIMAGE_NT_HEADERS)((PCHAR)tmp + ((PIMAGE_DOS_HEADER)tmp)->e_lfanew);
		auto imp = (PIMAGE_IMPORT_DESCRIPTOR)((PCHAR)tmp + nthdr->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		while (imp->Characteristics != 0) {
			std::string trash((PCHAR)((PCHAR)tmp + imp->Name));
			std::wstring processed = convrt.from_bytes(trash);
			imports.push_back(processed);
			++imp;
		}
		FreeLibrary(tmp);
		return imports;
	}


	void runStuff(DWORD wait=600)
	{
		std::vector<std::thread> threads;
		std::array<std::wstring, 10> procs = {
			L"xpsrchvw.exe",
			L"cmd /c nslookup google.com",
			L"mstsc.exe",
			L"mmc.exe",
			L"calc.exe",
			L"write.exe",
			L"notepad.exe",
			L"cmd /c ipconfig /all",
			L"cmd /c ping /t localhost",
			L"cmd /c netstat -anp",
		};

		for (auto i : procs) {
			threads.push_back(std::thread([=](auto arg) {
				STARTUPINFOW		startInfo = { 0 };
				PROCESS_INFORMATION procInfo = { 0 };

				startInfo.cb = sizeof(startInfo);
				startInfo.dwFlags |= STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
				startInfo.wShowWindow = SW_HIDE;

				if (!CreateProcessW(nullptr,
					(LPWSTR)arg.c_str(),
					NULL,
					NULL,
					FALSE,
					CREATE_NO_WINDOW,
					NULL,
					NULL,
					&startInfo,
					&procInfo))
				{
					return;
				}

				WaitForSingleObject(procInfo.hThread, wait);
				TerminateProcess(procInfo.hProcess, 0xf00d);

			}, i));
		}

		for (auto it = threads.begin(); it != threads.end(); ++it)
			it->join();
	}

	void queryStuff(DWORD a=0)
	{
		std::vector<std::thread> threads;
		std::vector<std::exception_ptr> excptr;
		std::vector<std::string> fails;
		std::mutex failLock;
		std::mutex excLock;

		std::array<std::wstring, 7> dlls = {
			L"\\Windows\\System32\\kernel32.dll",
			L"\\Windows\\System32\\OLEAUT32.dll",
			L"\\Windows\\System32\\WS2_32.dll",
			L"\\Windows\\System32\\IPHLPAPI.dll",
			L"\\Windows\\System32\\ADVAPI32.dll",
			L"\\Windows\\System32\\SHELL32.dll",
			L"\\Windows\\System32\\GDI32.dll",
		};

		auto x = GetTickCount();
		srand(x);

		std::cout << "[*] Beginning thread setup..." << std::endl;
		for (auto i = 0; i < 30; ++i) {
			threads.push_back(std::thread([&]() {
				WCHAR buf[MAX_PATH + 1] = { 0 };
				WCHAR outbuf[MAX_PATH + 1] = { 0 };
				DWORD bytesRet = 0;

				try {
					memset(buf, rand_in_range(rand()), sizeof(WCHAR) * MAX_PATH);
					auto hDev = CreateFileA("\\\\.\\EvalDriver", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_DEVICE, NULL);
					if (INVALID_HANDLE_VALUE == hDev) {
						throw std::exception(("Unable to open device! Error: " + std::to_string(GetLastError())).c_str());
					}

					if (!DeviceIoControl(hDev, EVAL_IMGNAME_IOCTL, buf, sizeof(WCHAR) * MAX_PATH, outbuf, sizeof(WCHAR) * MAX_PATH, &bytesRet, NULL)) {
						auto status = GetLastError();
						if (626 != status) {
							std::unique_lock<std::mutex> lock(failLock);
							fails.push_back("[x] Incorrectly handled invalid image query! Status: " + std::to_string(status));
						}
					}
					else {
						std::unique_lock<std::mutex> lock(failLock);
						fails.push_back("[x] Incorrectly handled invalid image query (success was returned)");
					}

					CloseHandle(hDev);
				}
				catch (...) {
					std::unique_lock<std::mutex> lock(excLock);
					auto exc = std::current_exception();
					excptr.push_back(exc);
				}
			}));
		}

		for (auto thr = threads.begin(); thr != threads.end(); ++thr) 
			thr->join();

		for (auto i : dlls) {
			[&](auto ws) {
				WCHAR buf[MAX_PATH + 1] = { 0 };
				WCHAR inbuf[MAX_PATH + 1] = { 0 };
				std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> convrt;
				DWORD bytesRet = 0;
				try {
					auto hDev = CreateFileA("\\\\.\\EvalDriver", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_DEVICE, NULL);
					if (INVALID_HANDLE_VALUE == hDev) {
						throw std::exception(("Unable to open device! Error: " + std::to_string(GetLastError())).c_str());
					}
					_snwprintf_s(inbuf, MAX_PATH, L"%s", ws.c_str());
					if (!DeviceIoControl(hDev, EVAL_IMGNAME_IOCTL, (PVOID)inbuf, sizeof(WCHAR) * MAX_PATH + 1, buf, sizeof(WCHAR) * MAX_PATH, &bytesRet, NULL)) {
						auto status = GetLastError();
						std::unique_lock<std::mutex> lock(failLock);
						fails.push_back("[x] Test failed! Didn't find expected image name! " + convrt.to_bytes(ws));
					}
					CloseHandle(hDev);
				}
				catch (...) {
					std::unique_lock<std::mutex> lock(excLock);
					auto exc = std::current_exception();
					excptr.push_back(exc);
				}
			}(i);
		}


		
		if (!excptr.empty()) {
			std::rethrow_exception(excptr.front());
		}

		if (!fails.empty()) {
			for (auto i : fails)
				printError(i);
		}
		else {
			printSuccess("[*] Test completed successfully!");
		}
	}


	void lastTest()
	{
		std::wstring fname = L"\\Windows\\System32\\WS2_32.dll";
		WCHAR buf[MAX_PATH + 1] = { 0 };
		WCHAR inbuf[MAX_PATH + 1] = { 0 };
		ULONG bytes;
		ULONG status = 0;

		auto hDev = CreateFileA("\\\\.\\EvalDriver", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_DEVICE, NULL);
		if (INVALID_HANDLE_VALUE == hDev)
			throw std::exception(("Unable to open device! Error: " + std::to_string(GetLastError())).c_str());

		_snwprintf_s(inbuf, MAX_PATH, L"%s", fname.c_str());

		if (!DeviceIoControl(hDev, EVAL_EMPTYQUEUE_IOCTL, NULL, 0, NULL, 0, &bytes, NULL)) {
			printError("[x] Something went wrong when processing IOCTL to empty queue!");
			CloseHandle(hDev);
			return;
		}

		if (!DeviceIoControl(hDev, EVAL_IMGNAME_IOCTL, inbuf, sizeof(WCHAR) * (MAX_PATH + 1), buf, sizeof(WCHAR) * MAX_PATH, &bytes, NULL) && (status = GetLastError()) == 626) {
			printSuccess("[*] Test passed... successfully removed items from queue after EMPTYQUEUE ioctl.");
		}
		else {
			printError("[x] Test failed, items not removed from queue after EMPTYQUEUE ioctl.");
		}
	}


	void printError(std::string msg)
	{
		char buf[81] = { 0 };
		memset(buf, '-', 80);
		SetConsoleTextAttribute(gConHandle, FOREGROUND_RED | FOREGROUND_INTENSITY);
		std::cout << std::endl << buf <<  msg << std::endl << buf << std::endl;
		resetAttributes();
	}

	void printSuccess(std::string msg)
	{
		char buf[81] = { 0 };
		memset(buf, '-', 80);
		SetConsoleTextAttribute(gConHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		std::cout << std::endl << buf <<  msg << std::endl << buf << std::endl;
		resetAttributes();
	}
}