$user = 'vagrant'
$pass = 'vagrant'
$secure_pass = ConvertTo-SecureString -AsPlainText $pass -Force
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $user, $secure_pass

Invoke-Command -ComputerName 192.168.33.12 -Credential $cred -scriptblock {& cmd /c "sc stop EvalDriver"}
cp .\x64\Debug\EvalDriver.sys \\WINDRIVER\EvalDriver\
Invoke-Command -ComputerName 192.168.33.12 -Credential $cred -scriptblock {& cmd /c "sc start EvalDriver"}
