#include <ntifs.h>
#include <stdio.h>
#include "DriverDefs.h"

#define EVAL_DEVICE_NAME		L"\\Device\\EvalDriver"
#define EVAL_SYM_NAME			L"\\??\\EvalDriver"
#define EVAL_IMGNAME_IOCTL		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define EVAL_EMPTYQUEUE_IOCTL	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)



namespace {
	PDRIVER_OBJECT				gDriver;
	PDEVICE_OBJECT				gDevice;

	// Forward declarations - dispatch functions
	DRIVER_DISPATCH				evalDriverIoctl;
	DRIVER_DISPATCH				evalDriverCreateDel;

	VOID evalNotify(PUNICODE_STRING, HANDLE, PIMAGE_INFO);
	DRIVER_UNLOAD				evalDriverUnload;

}

/**
*	This task will require the following:
* -> First, you will need to store the image names of all loaded modules
* -> Second, when you receive requests from the user mode application, you will
* be required to provide either the requested image name, or STATUS_NO_MORE_MATCHES
* (0xC0000273) if the name is not found.
* -> This task will require synchronization, as the image loads and data requests
* may happen simultaneously.
*/
extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT pDrv, PUNICODE_STRING pReg)
{
	UNICODE_STRING		deviceName	= { 0 };
	UNICODE_STRING		symName		= { 0 };
	NTSTATUS			status		= STATUS_SUCCESS;
	UNREFERENCED_PARAMETER(pReg);
	//DbgBreakPoint();
	xDebugPrint("Now Entering DriverEntry.");
	xPrintFixups("Fixup Symbols");
	RtlInitUnicodeString(&deviceName, EVAL_DEVICE_NAME);
	RtlInitUnicodeString(&symName, EVAL_SYM_NAME);

	gDriver = pDrv;
	gDevice = NULL;

	__try {
		// Device and symlink setup. For the purposes of this eval, we will only have one device object to deal with.
		if (!NT_SUCCESS((status = IoCreateDevice(pDrv, sizeof(EVAL_DRIVER_EXTENSION), &deviceName, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &gDevice)))) {
			xDebugPrint("Failed to initialize device with code: 0x%x", status);
			__leave;
		}

		if (!NT_SUCCESS((status = IoCreateSymbolicLink(&symName, &deviceName)))) {
			xDebugPrint("Failed to create symlink for device %wZ with code 0x%x", &deviceName, status);
			__leave;
		}

		if (!NT_SUCCESS((status = PsSetLoadImageNotifyRoutine(evalNotify)))) {
			xDebugPrint("Failed to register for image load notifications with status 0x%x", status);
			__leave;
		}

		//
		// TODO: Your code here
		//

		pDrv->DriverUnload = evalDriverUnload;

		pDrv->MajorFunction[IRP_MJ_DEVICE_CONTROL] = evalDriverIoctl;
		pDrv->MajorFunction[IRP_MJ_CREATE] = evalDriverCreateDel;
		pDrv->MajorFunction[IRP_MJ_CLOSE] = evalDriverCreateDel;

	}
	__finally {
		if (STATUS_SUCCESS != status && NULL != gDevice) {
			xDebugPrint("Cleaning up...");
			IoDeleteSymbolicLink(&symName);
			IoDeleteDevice(gDevice);
		}
	}

	xDebugPrint("Now exiting DriverEntry.");
	return status;
}

namespace {

	// ImageLoadNotify callback
	VOID evalNotify(PUNICODE_STRING pImgName, HANDLE procId, PIMAGE_INFO pImageInfo)
	{
		//UNREFERENCED_PARAMETER(pImgName);
		UNREFERENCED_PARAMETER(procId);
		UNREFERENCED_PARAMETER(pImageInfo);
		UNREFERENCED_PARAMETER(pImgName);
		
		//xDebugPrint("%wZ\n", pImgName);

		//
		// TODO: Your code here
		//
	}

	NTSTATUS evalDriverCreateDel(PDEVICE_OBJECT pDev, PIRP pIrp)
	{
		NTSTATUS status = STATUS_SUCCESS;

		UNREFERENCED_PARAMETER(pDev);
		xDebugPrint("Entering Create function");

		pIrp->IoStatus.Status = status;
		pIrp->IoStatus.Information = 0;

		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return status;
	}

	// DeviceIoControl entry point
	NTSTATUS evalDriverIoctl(PDEVICE_OBJECT pDev, PIRP pIrp)
	{
		PEVAL_DRIVER_EXTENSION	ext		= (PEVAL_DRIVER_EXTENSION)pDev->DeviceExtension;
		PIO_STACK_LOCATION		current = IoGetCurrentIrpStackLocation(pIrp);
		NTSTATUS				status	= STATUS_SUCCESS;

		UNREFERENCED_PARAMETER(ext);
		UNREFERENCED_PARAMETER(current);

		xDebugPrint("Entering IOCTL entry point...");

		//
		// TODO: Your code here
		//

		pIrp->IoStatus.Status = status;
		pIrp->IoStatus.Information = 0;

		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return status;
	}

	VOID evalDriverUnload(PDRIVER_OBJECT pDrv)
	{
		UNICODE_STRING	symName = { 0 };

		UNREFERENCED_PARAMETER(pDrv);

		xDebugPrint("Now entering DriverUnload.");

		RtlInitUnicodeString(&symName, EVAL_SYM_NAME);

		if (NULL != gDevice) {
			IoDeleteSymbolicLink(&symName);
			IoDeleteDevice(gDevice);
			PsRemoveLoadImageNotifyRoutine(evalNotify);
		}
		
		xDebugPrint("Done. Exiting...");
	}
}