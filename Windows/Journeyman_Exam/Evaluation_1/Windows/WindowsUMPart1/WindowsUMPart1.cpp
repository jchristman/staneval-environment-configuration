#include <Windows.h>
#include <string>
#include <stdio.h>

/* Test 1 definitions */
typedef int(*FileFunc)();
typedef PCHAR(*RegCheck)();

#define FILE_PATH "TempFile.txt"
#define SUBKEY_NAME "roflcopters\\bbqoneeleven"
#define KEY_ROOT HKEY_CURRENT_USER

/**
* File to Registry:
* - You have been tasked to take the output of one portion of the application, which is logged
* to a file, and copy it to a location in the registry where it will be accessible by another
* method, and then call it, to indicate that the task has been completed.
*
* In order to accomplish this task, you will need to do the following:
* - find the exported functions (WindowsGrader.dll) "fileSet" and "regCheck", which match the definitions for
* FileFunc and RegCheck (respectively)
* - Call fileSet
* - Copy the data from the file created by fileSet (FILE_PATH... #define'd above) to default value under
* the registry path SUBKEY_NAME under HKEY_CURRENT_USER.
* - Once done, call regCheck, which should return a pointer to the key (if successful), or NULL if not.
*/
void test1()
{

}

/**
* All of the tests in this section require functions that are all provided via exports from
* the WindowsGrader DLL (provided by test proctor). In order to successfully accomplish the objectives,
* the DLL will need to be loaded, and all relevant functions will need to be located.
*/
int wmain(int argc, WCHAR** argv, WCHAR** envp)
{
	test1();

	return 0;
}
