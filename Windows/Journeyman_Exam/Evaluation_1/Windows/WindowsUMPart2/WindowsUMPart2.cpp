#include <Windows.h>
#include <stdio.h>
#include <ImageHlp.h>
#include <iostream>
#include <string>
#include "defs.h"
#include <TlHelp32.h>

#pragma comment(lib, "WindowsUMPart2Grader.lib")
extern "C" BOOL WINAPI CheckErrorMessage(LPSTR msg, NTSTATUS code);
extern "C" BOOL WINAPI AddToExports(DWORD rva);
extern "C" BOOL WINAPI CheckFindModules();

static void displayError(PCHAR msg)
{
	MessageBoxA(NULL, msg, "ERROR", MB_OK | MB_ICONERROR);
	exit(GetLastError());
}

/**
* Utilizing the Windows Native API method
* RtlNtStatusToDosError (pointer declared below),
* convert the NTSTATUS error into a standard DOS/
* WINAPI error code (understandable by FormatMessage).
*
* Pass the resulting message and NT error code to CheckErrorMessage
* for validation.
*/
void nativeApi()
{
	PCHAR errorBuf = NULL;
	DWORD errorCode = 0;
	DWORD(WINAPI *RtlNtStatusToDosError)(NTSTATUS) = NULL;
	NTSTATUS ntError = 3221226094;

	/* Begin Implementation */


	/* End Implementation */
	if (!FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, 0, (LPSTR)&errorBuf, 0, NULL)) {
		printf("Failed to format message! Error: %d\n", GetLastError());
		return;
	}

	if (!CheckErrorMessage(errorBuf, ntError))
		printf("Error message validation failed! %d\n", GetLastError());
	LocalFree(errorBuf);
}

/**
* This task will require you to walk a portion of the export table
* in the "kernel32.dll" module, adding the RVA of the "GetProcAddress"
* exported function to the list of exports the grader will check against,
* via AddToExports(DWORD), (defined above). Once this is accomplished, use
* your GetProcAddress to write a message to a file using the given info.
* Then call the CheckFindModules() to trigger the grader to validate your inputs,
* and display the results via stdout.
*
* In summary:
*
* -> Locate the export rva of "GetProcAddress"
* -> Add the rva to the grader's list, via AddToExports(DWORD)
* -> Use your GetProcAddress to find CreateFileA and WriteFile.
* -> Write the message to the file and check with CheckFindModules()
*
**/

void findModules()
{	
	typedef FARPROC(WINAPI *MyGetProcAddress_t)(HMODULE, const char*);
	typedef HANDLE(WINAPI *MyCreateFile_t)(LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE);
	typedef BOOL(WINAPI *MyWriteFile_t)(HANDLE, LPCVOID, DWORD, LPDWORD, LPOVERLAPPED);

	LPCSTR filename = "..\\grader.txt";
	LPCSTR message = "Hello World!";
	HANDLE hFile = INVALID_HANDLE_VALUE;
	DWORD written = 0;
	DWORD funcRVA;
	
	HMODULE mod = GetModuleHandle("kernel32.dll");
	/* Begin Implementation */

	// if(!AddToExports(DWORD)) { }
	
	/* End Implementation */
	if (!CheckFindModules())
		printf("validation failed! %d\n", GetLastError());
}

// Uncomment each function when you are ready to run it.
int main(int argc, char** argv, char** envp)
{
	int status = ERROR_SUCCESS;
	//nativeApi();
	findModules();
	return status;
}
