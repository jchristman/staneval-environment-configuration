@echo off

sc stop evaldrv
sc delete evaldrv
del C:\EvalDriver.sys
move /y EvalDriver.sys C:\EvalDriver.sys
sc create evaldrv type= kernel start= demand binPath= C:\EvalDriver.sys
sc start evaldrv
pause
sc stop evaldrv
