@Echo off
:Start
@setlocal enableextensions
cd /d "%~dp0"

VulnEvalServer.exe
echo Program terminated at %Date% %Time% with Error %ErrorLevel%
echo Press Ctrl-C if you don't want to restart automatically
ping -n 10 localhost

goto Start