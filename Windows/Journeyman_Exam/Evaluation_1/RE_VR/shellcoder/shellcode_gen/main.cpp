#include <Windows.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>

namespace {

	const SIZE_T page_size = 15;

	template <typename T>
	void printBytes(T start, SIZE_T size) 
	{
		std::stringstream	ss;
		SIZE_T				pages = size / page_size;
		SIZE_T				current = 0;
		SIZE_T				count = 0;

		ss << std::hex << std::setfill('0');

		for(SIZE_T i = 0; i < size; ++i, ++count) {
			if (page_size == count) {
				++current;
				count = 0;
				ss << "\"\\" << std::endl << "      \"";
			}

			ss << "\\x" << std::setw(2) << static_cast<unsigned int>(start[i]);
		}
		std::cout << std::endl << "shell=\"" << ss.str() << "\"" << std::endl;
	}
}

extern "C" BYTE		codeStart[];
extern "C" SIZE_T	codeSize;
extern "C" DWORD	firstHashedFunc;
extern "C" DWORD	hashString(const char*);

int main(int argc, char** argv, char** envp)
{
	int status = ERROR_SUCCESS;
	DWORD old;

	VirtualProtect(&firstHashedFunc, sizeof(DWORD), PAGE_EXECUTE_READWRITE, &old);

	firstHashedFunc = hashString("CreateFileA");


	printBytes(codeStart, codeSize);

	return status;
}