#include <ntifs.h>
#include <stdio.h>
#include "DriverDefs.h"

#define EVAL_DEVICE_NAME		L"\\Device\\EvalDriver"
#define EVAL_SYM_NAME			L"\\??\\EvalDriver"
#define EVAL_IMGNAME_IOCTL		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define EVAL_EMPTYQUEUE_IOCTL	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)

//STUDENT CODE
#define MUTEX_NAME				L"mahmutex"
//STUDENT CODE


namespace {
	PDRIVER_OBJECT				gDriver;
	PDEVICE_OBJECT				gDevice;

	// Forward declarations - dispatch functions
	DRIVER_DISPATCH				evalDriverIoctl;
	DRIVER_DISPATCH				evalDriverCreateDel;

	VOID evalNotify(PUNICODE_STRING, HANDLE, PIMAGE_INFO);
	DRIVER_UNLOAD				evalDriverUnload;

	//STUDENT CODE
	LIST_ENTRY headNode;
	typedef struct _imgNamesStruct {
		LIST_ENTRY le;
		UNICODE_STRING name;
	} IMG_NAMES, *PIMG_NAMES;
	PFAST_MUTEX mahMutex;
	//STUDENT CODE
}

/**
*	This task will require the following:
* -> First, you will need to store the image names of all loaded modules
* -> Second, when you receive requests from the user mode application, you will
* be required to provide either the requested image name, or STATUS_NO_MORE_MATCHES
* (0xC0000273) if the name is not found.
* -> This task will require synchronization, as the image loads and data requests
* may happen simultaneously.
*/
extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT pDrv, PUNICODE_STRING pReg)
{
	UNICODE_STRING		deviceName	= { 0 };
	UNICODE_STRING		symName		= { 0 };
	NTSTATUS			status		= STATUS_SUCCESS;
	UNREFERENCED_PARAMETER(pReg);
	//DbgBreakPoint();
	xDebugPrint("Now Entering DriverEntry.");
	xPrintFixups("Fixup Symbols");
	RtlInitUnicodeString(&deviceName, EVAL_DEVICE_NAME);
	RtlInitUnicodeString(&symName, EVAL_SYM_NAME);

	gDriver = pDrv;
	gDevice = NULL;

	__try {
		// Device and symlink setup. For the purposes of this eval, we will only have one device object to deal with.
		if (!NT_SUCCESS((status = IoCreateDevice(pDrv, sizeof(EVAL_DRIVER_EXTENSION), &deviceName, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &gDevice)))) {
			xDebugPrint("Failed to initialize device with code: 0x%x", status);
			__leave;
		}

		if (!NT_SUCCESS((status = IoCreateSymbolicLink(&symName, &deviceName)))) {
			xDebugPrint("Failed to create symlink for device %wZ with code 0x%x", &deviceName, status);
			__leave;
		}

		//STUDENT CODE
		mahMutex = (PFAST_MUTEX)ExAllocatePool(NonPagedPool, sizeof(FAST_MUTEX));
		ExInitializeFastMutex(mahMutex);
		InitializeListHead(&headNode);
		//STUDNET CODE

		if (!NT_SUCCESS((status = PsSetLoadImageNotifyRoutine(evalNotify)))) {
			xDebugPrint("Failed to register for image load notifications with status 0x%x", status);
			__leave;
		}

		pDrv->DriverUnload = evalDriverUnload;

		pDrv->MajorFunction[IRP_MJ_DEVICE_CONTROL] = evalDriverIoctl;
		pDrv->MajorFunction[IRP_MJ_CREATE] = evalDriverCreateDel;
		pDrv->MajorFunction[IRP_MJ_CLOSE] = evalDriverCreateDel;

	}
	__finally {
		if (STATUS_SUCCESS != status && NULL != gDevice) {
			xDebugPrint("Cleaning up...");
			IoDeleteSymbolicLink(&symName);
			IoDeleteDevice(gDevice);
		}
	}

	xDebugPrint("Now exiting DriverEntry.");
	return status;
}

namespace {

	// ImageLoadNotify callback
	VOID evalNotify(PUNICODE_STRING pImgName, HANDLE procId, PIMAGE_INFO pImageInfo)
	{
		//UNREFERENCED_PARAMETER(pImgName);
		UNREFERENCED_PARAMETER(procId);
		UNREFERENCED_PARAMETER(pImageInfo);
		
		//xDebugPrint("%wZ\n", pImgName);
		// Start: student code
		PIMG_NAMES curName = NULL;
		PLIST_ENTRY curNode = NULL;
		int isFound = FALSE;
		curName = (PIMG_NAMES)ExAllocatePool(NonPagedPool, sizeof(IMG_NAMES));
		curName->name.Buffer = (PWCHAR)ExAllocatePool(NonPagedPool, pImgName->Length + (sizeof(WCHAR)));
		curName->name.MaximumLength = pImgName->Length + sizeof(WCHAR);
		curName->name.Length = 0;
		RtlCopyUnicodeString(&(curName->name), pImgName);
		_wcslwr(curName->name.Buffer);
		//xDebugPrint("%wZ\n", &(curName->name));
		ExAcquireFastMutex(mahMutex);
		//DbgBreakPoint();
		for (curNode = headNode.Flink; curNode != &headNode; curNode = curNode->Flink) {
			//xDebugPrint("Curnode: %wZ\n", &(CONTAINING_RECORD(curNode, IMG_NAMES, le)->name));
			if (wcscmp(curName->name.Buffer, CONTAINING_RECORD(curNode, IMG_NAMES, le)->name.Buffer) == 0) {
				//DbgBreakPoint();
				isFound = TRUE;
				break;
			}
		}
		if (!isFound) {
			InsertHeadList(&headNode, &(curName->le));
		}
		ExReleaseFastMutex(mahMutex);
		if (isFound) {
			ExFreePool(curName->name.Buffer);
			ExFreePool(curName);
		}
		// End: student code
	}

	NTSTATUS evalDriverCreateDel(PDEVICE_OBJECT pDev, PIRP pIrp)
	{
		NTSTATUS status = STATUS_SUCCESS;

		UNREFERENCED_PARAMETER(pDev);
		xDebugPrint("Entering Create function");

		pIrp->IoStatus.Status = status;
		pIrp->IoStatus.Information = 0;

		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return status;
	}

	//MYCODE
	NTSTATUS findImgName(PIRP pIrp) {
		NTSTATUS retVal = STATUS_NO_MORE_MATCHES;
		int isFound = FALSE;
		PWCHAR buff = (PWCHAR)pIrp->AssociatedIrp.SystemBuffer;
		PLIST_ENTRY curNode = NULL;
		PIMG_NAMES curName = NULL;
		_wcslwr(buff);
		xDebugPrint("Looking for: %ls\n", buff);
		//DbgBreakPoint();
		ExAcquireFastMutex(mahMutex);
		for (curNode = headNode.Flink; curNode != &headNode; curNode = curNode->Flink) {
			//xDebugPrint("Curnode: %wZ\n", &(CONTAINING_RECORD(curNode, IMG_NAMES, le)->name));
			curName = CONTAINING_RECORD(curNode, IMG_NAMES, le);
			if (wcscmp(buff, curName->name.Buffer) == 0) {
				xDebugPrint("Found %ls!\n", buff);
				isFound = TRUE;
				break;
			}
		}
		ExReleaseFastMutex(mahMutex);
		if (isFound) {
			retVal = STATUS_SUCCESS;
		}
		return retVal;
	}

	VOID removeImgNames() {
		PLIST_ENTRY curNode = NULL;
		PIMG_NAMES curName = NULL;
		//DbgBreakPoint();
		ExAcquireFastMutex(mahMutex);
		while (!IsListEmpty(&headNode)) {
			curNode = RemoveHeadList(&headNode);
			curName = CONTAINING_RECORD(curNode, IMG_NAMES, le);
			ExFreePool(curName->name.Buffer);
			ExFreePool(curName);
		}
		ExReleaseFastMutex(mahMutex);

	}
	//MYCODE
	// DeviceIoControl entry point
	NTSTATUS evalDriverIoctl(PDEVICE_OBJECT pDev, PIRP pIrp)
	{
		PEVAL_DRIVER_EXTENSION	ext		= (PEVAL_DRIVER_EXTENSION)pDev->DeviceExtension;
		PIO_STACK_LOCATION		current = IoGetCurrentIrpStackLocation(pIrp);
		NTSTATUS				status	= STATUS_SUCCESS;

		UNREFERENCED_PARAMETER(ext);

		xDebugPrint("Entering IOCTL entry point...");

		// Start: student code
		if (EVAL_IMGNAME_IOCTL == current->Parameters.DeviceIoControl.IoControlCode) {
			status = findImgName(pIrp);
		}
		else if(EVAL_EMPTYQUEUE_IOCTL == current->Parameters.DeviceIoControl.IoControlCode) {
			removeImgNames();
		}
		// End: student code

		pIrp->IoStatus.Status = status;
		pIrp->IoStatus.Information = 0;

		IoCompleteRequest(pIrp, IO_NO_INCREMENT);
		return status;
	}

	VOID evalDriverUnload(PDRIVER_OBJECT pDrv)
	{
		UNICODE_STRING	symName = { 0 };

		UNREFERENCED_PARAMETER(pDrv);

		xDebugPrint("Now entering DriverUnload.");

		RtlInitUnicodeString(&symName, EVAL_SYM_NAME);

		if (NULL != gDevice) {
			IoDeleteSymbolicLink(&symName);
			IoDeleteDevice(gDevice);
			PsRemoveLoadImageNotifyRoutine(evalNotify);
		}
		
		xDebugPrint("Done. Exiting...");
	}
}