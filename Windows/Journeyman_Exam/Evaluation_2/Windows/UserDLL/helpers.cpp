#include "helpers.h"
#include <stdio.h>
#include <ImageHlp.h>


PVOID* findImportByName(HMODULE hm, LPCSTR importModuleName, LPCSTR name)
{
	PVOID* piatEntry = NULL;

	if (NULL == hm || NULL == name) {
		SetLastError(ERROR_INVALID_PARAMETER);
		return NULL;
	}

	auto tmp = (PIMAGE_IMPORT_DESCRIPTOR)((PCHAR)hm + ((PIMAGE_NT_HEADERS)((PCHAR)hm + ((PIMAGE_DOS_HEADER)hm)->e_lfanew))->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	while (tmp->Characteristics != 0) {
		if (0 == _stricmp(((PCHAR)hm + tmp->Name), importModuleName)) {
			break;
		}

		++tmp;
	}

	if (NULL == tmp->Characteristics) {
		// couldn't find the module
		SetLastError(ERROR_INVALID_NAME);
		return NULL;
	}

	auto thunkin = (PULONG)((PCHAR)hm + tmp->OriginalFirstThunk);
	auto iatModBase = (PULONG)((PCHAR)hm + tmp->FirstThunk);
	for (auto i = 0; NULL != thunkin[i]; ++i) {

		if (IMAGE_SNAP_BY_ORDINAL(thunkin[i]))
			continue;

		auto imgImport = (PIMAGE_IMPORT_BY_NAME)((PCHAR)hm + thunkin[i]);
		if (0 == strcmp(imgImport->Name, name)) {
			piatEntry = (PVOID*)(iatModBase[i]);
			break;
		}
	}

	return piatEntry;
}