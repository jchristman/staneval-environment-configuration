#include <Windows.h>
#include <stdio.h>
#include "helpers.h"
// THIS IS PART OF WindowsUMPart3 //

static BYTE patch[] = { 0xb0, 0x01, 0xc3 }; // return TRUE
static HMODULE gHmod = NULL;
typedef PCHAR(*GetKeyFunc)(DWORD);
typedef BOOL(*IsAuthorizedFunc)(DWORD);

/**
* Part of WindodwsUMPart3
* Once you've arrived in the target process's memory space, you now have a second task to accomplish:
* 
* -> There is a function exported by another DLL loaded into the grader's process (named "GraderDLL")
* called "getKey" that you will need to ultimately utilize to get the key.
*
* -> Unfortunately, however, this method will only return the key to authorized users (which it validates
* by calling the aptly named "isAuthorized" method).
*
* -> Your task at this point is to a.) force the "isAuthorized" method to return TRUE, and b.) call getKey,
* which should now be tricked into providing you with the key.
*/
void patchGetKey()
{

	if (NULL == (gHmod = GetModuleHandle("GraderDLL"))) {
		MessageBoxA(NULL, "Failed to get grader dll!\n", "s", MB_OK);
		return;
	}

	/* Your code here... */
	//printf("Attempt!\n");
	IsAuthorizedFunc af = (IsAuthorizedFunc)GetProcAddress(gHmod, "isAuthorized");
	BYTE* pB = ((BYTE*)af) + 0x33d;
	GetKeyFunc gk = (GetKeyFunc)GetProcAddress(gHmod, "getKey");
	DWORD oldStuff;
	if (!af) {
		MessageBoxA(NULL, "Failed to get af\n", "s", MB_OK);
	}
	//PCHAR myStr = gk(80);
	VirtualProtect(pB, 2048, PAGE_EXECUTE_READWRITE, &oldStuff);
	//MessageBoxA(NULL, myStr, "s", MB_OK);
	//printf("Data at %p:\n\t%02x\n", af, *(char*)af);
	//printf("Data at %p:\n\t%02x\n", pB, *(DWORD*)pB);

	memcpy(((pB) + 0x88), &patch, 3);
	//printf("Data at %p:\n\t%02x\n", pB+0x88, *(DWORD*)(pB+0x88));
	
	//BOOL test = TRUE;
	//test = af(0xde46);
	//MessageBoxA(NULL, "Got back!\n", "s", MB_OK);
	/*
	if (test) {
		MessageBoxA(NULL, "Authed!\n", "s", MB_OK);
	}
	else {
		MessageBoxA(NULL, "NotAuthed...\n", "s", MB_OK);
	}
	*/

	PCHAR str = gk(1);
	printf("String!\n\t%s\n", str);
	
}

/**
* Once you've been loaded, you should see a popup box with command line, which should validate that you successfully made it into
* the grader's process space.
*/
BOOL WINAPI DllMain(HINSTANCE hInst, DWORD dwReason, LPVOID lpRes)
{
	BOOL res = TRUE;
	CHAR buf[MAX_PATH + 31] = { 0 };

	switch (dwReason){
	case DLL_PROCESS_ATTACH:
#pragma warning(push)
#pragma warning(disable:4996)
		_snprintf(buf, MAX_PATH + 30, "Made it here! Command Line: %s\n", GetCommandLineA());
#pragma warning(pop)
		MessageBoxA(NULL, buf, "Made it!", MB_OK);
		patchGetKey();
	case DLL_THREAD_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return res;
}