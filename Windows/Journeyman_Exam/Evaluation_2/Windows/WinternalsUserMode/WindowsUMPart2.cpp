#include <Windows.h>
#include <stdio.h>
#include <ImageHlp.h>
#include <iostream>
#include <string>
#include "defs.h"

#pragma comment(lib, "WinternalsUserModeGrader.lib")
extern "C" BOOL WINAPI CheckErrorMessage(LPSTR msg, NTSTATUS code);

// Exports Task
extern "C" BOOL WINAPI AddToExportsList(PCHAR mod);
extern "C" BOOL WINAPI CheckExportsList();

// Module Task
extern "C" BOOL WINAPI AddToModuleList(PWCHAR modPath);
extern "C" BOOL WINAPI CheckModuleList();

static void displayError(PCHAR msg)
{
	MessageBoxA(NULL, msg, "ERROR", MB_OK | MB_ICONERROR);
	exit(GetLastError());
}

/**
* This task will require you to locate the paths to
* all of the modules currently loaded in the process (also referred to as
* the module's FullDllName).
* The preferred list to use should be the InIntializationOrderModuleList,
* which can be found via the PEB (structure definitions have been provided
* in the included "defs.h" header file).
*
* -> Once found, each path should be appended to the module list the grader
* checks against (via AddToModuleList(PWCHAR) ).
*
* -> Once the list has been enumerated, it should be checked against the grader
* by calling CheckModuleList(), which should display the results of the test
* via stdout.
*/

PPEB getPEB() {

	PPEB pPEB;
	__asm {
		mov EAX, FS:[0x30]
		mov pPEB, EAX
	}
	return pPEB;
}

void listModules()
{
	PPEB peb = NULL;

	/* Begin Implementation */
	PPEB_LDR_DATA ldrdata = NULL;
	PLIST_ENTRY head = NULL;
	PLIST_ENTRY cur = NULL;
	PWCHAR curName = NULL;
	peb = getPEB();
	ldrdata = peb->LoaderData;
	head = &(ldrdata->InInitializationOrderModuleList);
	for (cur = head->Flink; cur != head; cur = cur->Flink) {
		curName = CONTAINING_RECORD(cur, _LDR_MODULE, InInitializationOrderModuleList)->FullDllName.Buffer;
		if (!AddToModuleList(curName)) {}
	}

	/* End Implementation */
	if (!CheckModuleList())
		printf("Module list validation failed! %d\n", GetLastError());
}

/**
* This task will require you to walk a portion of the export table
* in the "WinternalsUserModeGrader.dll" module, adding the name of each
* exported function to the list of exports the grader will check against,
* via AddToExportsList(PCHAR), (defined above). Once this is accomplished,
* a call to CheckExportsList() will trigger the grader to validate your inputs,
* and display the results via stdout.
*
* In summary:
*
* -> Locate the names of all exports for "WinternalsUserModeGrader.dll"
* -> Add each name to the grader's list, via AddToExportsList(PCHAR)
* -> Once complete, call CheckExportsList() to validate input.
*/
void enumerateExports()
{
	PIMAGE_DOS_HEADER base = NULL;
	PIMAGE_NT_HEADERS next = NULL;
	PIMAGE_OPTIONAL_HEADER opts = NULL;
	PIMAGE_EXPORT_DIRECTORY exports = NULL;
	PDWORD nameBase = NULL;
	HMODULE mod = GetModuleHandle("WinternalsUserModeGrader.dll");

	/* Begin Implementation */
	DWORD imageBase = 0;
	PCHAR name = NULL;
	DWORD numNames = 0;
	DWORD i = 0;
	base = (PIMAGE_DOS_HEADER)mod;
	next = (PIMAGE_NT_HEADERS)((BYTE*)mod + base->e_lfanew);
	imageBase = next->OptionalHeader.ImageBase;
	exports = (PIMAGE_EXPORT_DIRECTORY)(imageBase + next->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
	nameBase = (PDWORD)(imageBase + exports->AddressOfNames);
	numNames = exports->NumberOfNames;

	for (i = 0; i < numNames; i++) {
		name = (PCHAR)(nameBase[i] + imageBase);
		if (!AddToExportsList(name)) {
			printf("bad\n");
		}
	}
	

	// if(!AddToExportsList(PCHAR)) {}

	/* End Implementation */
	if (!CheckExportsList())
		printf("Export validation failed! %d\n", GetLastError());

}

// Uncomment each function when you are ready to run it.
int main(int argc, char** argv, char** envp)
{
	int status = ERROR_SUCCESS;
	listModules();
	enumerateExports();
	return status;
}

