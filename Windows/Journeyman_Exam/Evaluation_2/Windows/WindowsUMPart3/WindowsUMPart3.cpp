#include <Windows.h>
#include <stdio.h>
#include "helpers.h"

#define DLL_NAME "WindowsUMPart3-UserDLL.dll"

extern "C" BYTE shellcode_start[];
extern "C" DWORD shellcode_size;
extern "C" DWORD lla;

static BYTE patch[] = { 0xb0, 0x01, 0xc3 }; // return TRUE
static HMODULE gHmod = NULL;
typedef PCHAR(*GetKeyFunc)(DWORD);
typedef BOOL(*IsAuthorizedFunc)(DWORD);

/**
* You have been tasked with injecting a DLL into a remote process. The process (run from a module named "grader.exe"... 
* you will need to launch it prior to executing the binary from this project) will need to be located, and your position
* independent shell code (once completed) will need to be injected and executed in the remote process, loading the 
* UserDLL you create into the grader's process. Once complete, a messagebox popup from grader.exe (displaying grader's
* commandline) will indicate success.
*/
int main(int argc, char** argv, char** envp)
{
	int status = ERROR_SUCCESS;
	DWORD pid = 0;
	HANDLE hProc;
	PBYTE buf;
	DWORD written;
	HANDLE hT;

	if (0 == (pid = getPidByName("grader.exe"))) {
		printf("Unable to find the grader running! Are you sure you started it?\n");
		system("pause");
		return 0;
	}

	/*
		Your code here...
	*/
	PVOID pLibRemote;
	PCHAR szLibPath = "C:\\Users\\DOV\\Desktop\\Checkride-V1-20160323\\Checkride-V1\\Windows\\Debug\\WindowsUMPart3-UserDLL.dll";
	HMODULE hK;
	PVOID fun;
	DWORD retCode;
	hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (hProc == NULL) {
		printf("Was problem getting handle..\n");
		exit(1);
	}

	pLibRemote = VirtualAllocEx(hProc, NULL, strlen(szLibPath) + 1, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);

	BOOL bRc = WriteProcessMemory(hProc, pLibRemote, (void*)szLibPath, strlen(szLibPath) + 1, NULL);
	hK = GetModuleHandle("Kernel32");
	fun = GetProcAddress(hK, "LoadLibraryA");
	hT = CreateRemoteThread(hProc, NULL, 0,
		(LPTHREAD_START_ROUTINE)fun,
		pLibRemote, 0, NULL);

	WaitForSingleObject(hT, INFINITE);

	bRc =GetExitCodeThread(hT, &retCode);
	/*
	if (NULL == (gHmod = GetModuleHandle("GraderDLL"))) {
		MessageBoxA(NULL, "Failed to get grader dll!\n", "s", MB_OK);
		return status;
	}

	 Your code here... 
	IsAuthorizedFunc af = (IsAuthorizedFunc)GetProcAddress(gHmod, "isAuthorized");
	GetKeyFunc gk = (GetKeyFunc)GetProcAddress(gHmod, "getKey");
	if (!af) {
		MessageBoxA(NULL, "Failed to get af\n", "s", MB_OK);
	}
	//PCHAR myStr = gk(80);

	//MessageBoxA(NULL, myStr, "s", MB_OK);

	memcpy((((BYTE*)af) + 0x2c), &patch, 3);

	BOOL test = af(0xde46);
	if (test) {
		MessageBoxA(NULL, "Authed!\n", "s", MB_OK);
	}
	else {
		MessageBoxA(NULL, "NotAuthed...\n", "s", MB_OK);
	}
	*/
	return status;
}
