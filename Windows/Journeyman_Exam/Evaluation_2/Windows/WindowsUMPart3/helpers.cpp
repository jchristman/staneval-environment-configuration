#include "helpers.h"

namespace {

}

DWORD getPidByName(PCHAR name)
{
	DWORD pid = 0;
	PROCESSENTRY32 pentry = { 0 };
	HANDLE hSnap;

	if (NULL == name)
		return pid; // bad input

	if (INVALID_HANDLE_VALUE == (hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0))) {
		printf("Error: Failed to get process ID! Last error: %d\n", GetLastError());
		return 0;
	}

	pentry.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnap, &pentry)) {
		printf("Error: Failed to get first process snapshot! Last error: %d\n", GetLastError());
		CloseHandle(hSnap);
		return 0;
	}

	if (!strcmp(pentry.szExeFile, name)) {
		CloseHandle(hSnap);
		return pentry.th32ProcessID;
	}

	while (Process32Next(hSnap, &pentry)) {
		if (!strcmp(pentry.szExeFile, name)) {
			pid = pentry.th32ProcessID;
			break;
		}
	}

	if (0 == pid)
		printf("Error: process wasn't found!\n");

	CloseHandle(hSnap);
	return pid;
}