#pragma once

#include <Windows.h>
#include <TlHelp32.h>
#include <stdio.h>

typedef struct {
	PVOID origEntry;
	PVOID bounce;
} TRAMPOLINE, *PTRAMPOLINE;

DWORD getPidByName(PCHAR name);