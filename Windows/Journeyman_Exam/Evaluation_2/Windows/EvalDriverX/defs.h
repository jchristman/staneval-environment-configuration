#pragma once

#include <ntifs.h>

// Dummy device extension if additional storage is
// desired
typedef struct _EVAL_DRIVER_EXTENSION {
	PDEVICE_OBJECT pAttachedDev;
} EVAL_DRIVER_EXTENSION, *PEVAL_DRIVER_EXTENSION;

// DbgPrint with some decoration
#if DBG

#define xDebugPrint(x, ...)					DbgPrint("<?dml?><col fg=\"changed\">[EvalDriver] - %s : %lu - " x "</col>\n", __FILE__, __LINE__, __VA_ARGS__)
#define xPrintFixups(lname)					DbgPrint("<?dml?><exec cmd=\"!sym noisy; .symfix; .reload\">" lname "</exec>\n")

#else

#define xDebugPrint(x, ...)		(VOID*)0
#define xPrintFixups(lname)		(VOID*)0

#endif

