1. Copy the driver to the target machine:
```
cp ..\x64\Debug\EvalDriverX.sys \\WINDRIVER\EvalDriver
```
2. Load the driver:
On the target machine:
```
sc start EvalDriverX
```
