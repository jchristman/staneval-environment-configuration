#include <Windows.h>
#include <string>
#include <stdio.h>

/* Test 1 Function Definitions */
typedef DWORD(*SharedFunc)(PDWORD);
typedef PCHAR(WINAPI *TestFunc1)(SharedFunc, SharedFunc);

/**
* Thread Synchronization:
* - You have been asked to implement a mechanism for a multithreaded application to
* manage reference counts for a shared resource. The current requirement is to implement
* two functions (matching the SharedFunc prototype, above): one that will increment the
* contended resource in a thread-safe manner, and one that will decrement the resource in
* a thread-safe manner.
*
* - In order to complete this task, you will need to:
* 1.) Load the WindowsGrader.dll
* 2.) Get a pointer to the exported threadSync function (which matches the TestFunc1 definition at
* the top of the file)
* 3.) Call threadSync, providing your increment and decrement functions as arguments (the first 
* argument being your increment implementation, and the second being decrement). 
*
* - If the test succeeds, you will get back a string containing the key. 
* If it fails however, (either because the operations are not altering the resource, or because
* they are not thread safe) you will get back a NULL pointer.
*/

DWORD incer(PDWORD var) {
	InterlockedIncrement(var);
	return *var;
}

DWORD decer(PDWORD var) {
	InterlockedDecrement(var);
	return *var;
}

void test1()
{
	/*
	Your code here...
	*/

	LPCWSTR fName = L"C:\\Users\\DOV\\Desktop\\Checkride-V1-20160323\\Checkride-V1\\Windows\\WindowsGrader.dll";
	HMODULE hWG;
	TestFunc1 tf;
	PCHAR retVal = NULL;
	hWG = LoadLibraryW(fName);

	tf = (TestFunc1)GetProcAddress(hWG, "_threadSync@8");

	if (!tf) {
		DWORD err = GetLastError();
		printf("Err: %u\n", err);
	}

	retVal = tf(incer, decer);

	if (!retVal) {
		printf("Danger will robinson!\n");
	}
	else {
		printf("%s\n", retVal);
	}


	return;


}


/**
* All of the tests in this section require functions that are all provided via exports from
* the WindowsGrader.dll (provided by test proctor). In order to successfully accomplish the objectives,
* the DLL will need to be loaded, and all relevant functions will need to be located.
*/
int wmain(int argc, WCHAR** argv, WCHAR** envp)
{
	test1();

	return 0;
}
