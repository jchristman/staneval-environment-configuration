/////////////////////////////////////////
/// Vulnerability Research
/// 
/////////////////////////////////////////

Eval Directions:
================

- You've been provided with an application
that suffers from a vulnerability in how it
parses its configuration file (which it
expects to find in the current user's profile
directory).

Part 1:
-------
Using static and dynamic analysis, in 
addition to the tools provided, such as the 
config gen tool (to assist with fuzzing), and
the sample "good" config, perform the following:

-   Locate the vulnerable condition
-   Generate malformed input that will lead to
    arbitrary code execution.

Part 2:
-------
Using the vulnerability located in Part 1, 
perform the following steps:

-   Generate position independent shellcode,
    using the shellcode generation VS solution
    that will create an empty file (the name can
    be arbitrary).

    a.) A position independent getprocaddress-
    from-kernel32 method has been provided for 
    use to obtain a pointer to CreateFile(A|W)
    once your code begins to run.
    b.) The parameter it expects is a hashed
    value (which can be generated via the other
    method in the same file).
    c.) An important note: both of these functions
    are __cdecl (thus, you will have to clean up
    the stack after they return).

-   Leverage the exploit to inject and run your
    code via lister.exe, and ensure a file gets
    created.