bits 32

section .text

global _codeStart, _codeSize, _firstHashedFunc

_codeStart:
	pushad
	jmp logicEnd

	%include "Win32FindFuncs.S" ; Provides: void* __cdecl findK32Func(DWORD) (e.g., call _findK32Func)
								; which should find an arbitrary export from Kernel32.
								; The DWORD it takes is a string hashed via DWORD __cdecl hashString(const char*)
								; (e.g., call _hashString)... also provided.

logicStart:
	pop esi
	lodsd

	push _firstHashedFunc
	call _findK32Func
	add esp, 4
	xor ecx, ecx
	push ecx
	xor ebx, ebx
	mov bl, 80
	push ebx
	mov bl, 2
	push ebx
	push ecx
	mov ebx, 0xc1111111
	sub ebx, 0x01111111
	push ebx
	push _fileName
	call eax
	popad
	ret
logicEnd:
	call logicStart

_firstHashedFunc	dd		0x00
_fileName db "C:\\f.txt\0"
_codeSize	dd	$-_codeStart