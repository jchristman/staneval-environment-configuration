﻿# Apply the license to VS
$VSlicense = "KHJ4N-892F7-JJMJT-7GC7P-9QCDH 08860"
& "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\StorePID.exe" $VSlicense

#Open Eval Instructions
$path = "C:\vagrant\Exam\resources\EvalInstructions.txt"
$status = Test-Path $path -PathType Leaf
if ($status -ne "") {
notepad.exe "C:\vagrant\Exam\resources\EvalInstructions.txt"
}

#Open sucess jpg
$path = "C:\vagrant\Exam\resources\Eval Passed Test Examples.jpg"
$status = Test-Path $path -PathType Leaf
if ($status -ne "") {
mspaint.exe "C:\vagrant\Exam\resources\Eval Passed Test Examples.jpg"
}

#Create link to vagrant sync Exam folder (NOT WOKRING)
#New-Item -ItemType SymbolicLink -Path "C:\Users\vagrant\Exam" -Value "C:\Users\vagrant\Desktop\Exam"
Copy-Item -Path "C:\vagrant\Exam" -Destination "C:\Users\vagrant\Desktop\Exam" -recurse -Force

#Copy source code to Desktop\Exam
$path = "C:\Users\vagrant\Desktop\Exam"
$status = Test-Path $path -PathType Leaf
if ($status -ne "") {
start "C:\Users\vagrant\Desktop\Exam"
}

Start-Process "chrome.exe" "http://192.168.10.100:3000"
