
# Apply the visual studio license key
& "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\Common7\\IDE\\StorePID.exe" KHJ4N-892F7-JJMJT-7GC7P-9QCDH 08862

# Add the driver machine to the list of trusted hosts
winrm set winrm/config/client '@{TrustedHosts="WINDRIVER,192.168.33.12"}'

# Install the WDK VS extension
wget "http://" + $url + ":9003/images/WDK.vsix" -UseBasicParsing -OutFile c:\\users\\vagrant\\wdk.vsix
& "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\Common7\\IDE\\VSIXInstaller.exe" /a /q c:\\users\\vagrant\\wdk.vsix

#Install CFF Explorer
choco.exe install explorersuite -y

#Install PE Viewer
wget http://wjradburn.com/software/PEview.zip -UseBasicParsing -OutFile c:\\users\\vagrant\\PEViewer.zip
Expand-Archive c:\\users\\vagrant\\PEViewer.zip -DestinationPath c:\\users\\vagrant\\Desktop
rm -Force c:\\users\\vagrant\\PEViewer.zip

#Install OLLYDBG
wget http://www.ollydbg.de/odbg110.zip -UseBasicParsing -OutFile c:\\users\\vagrant\\OLLY.zip
New-Item -ItemType directory -Path c:\\users\\vagrant\\OllyDBG
Expand-Archive c:\\users\\vagrant\\OLLY.zip -DestinationPath c:\\users\\vagrant\\OllyDBG
rm -Force c:\\users\\vagrant\\OLLY.zip

#Install adobe reader
Invoke-WebRequest "http://ardownload.adobe.com/pub/adobe/reader/win/AcrobatDC/1502320053/AcroRdrDC1502320053_en_US.exe" -OutFile "c:\\users\\vagrant\\adobe.exe"
Start-Process -FilePath "c:\\users\\vagrant\\adobe.exe" -ArgumentList "/sPB /rs"
rm -Force c:\\users\\vagrant\\adobe.exe
   
#Install sysinternals suite  https://download.sysinternals.com/files/SysinternalsSuite.zip
Invoke-WebRequest "https://download.sysinternals.com/files/SysinternalsSuite.zip" -OutFile "c:\\users\\vagrant\\SysinternalsSuite.zip"
New-Item -ItemType directory -Path c:\\users\\vagrant\\SysinternalSuite
Expand-Archive "c:\\users\\vagrant\\SysinternalsSuite.zip" -DestinationPath c:\\users\\vagrant\\SysinternalSuite
rm -Force c:\\users\\vagrant\\SysinternalsSuite.zip

# Make directory for symbols for local cache
mkdir c:\\windows\\symbols