import inspect, base64, re

t = """ZjE4NzlmNGZhMTMyMmE0MzUxMjFlMjI1Mjk0Zjg1NjQ4Y2M5MzY5YjgyNWNlYTdmZWM1MmNiZGQ1YjlhNDFiMDdiNWQ1YzQwODk3MTQ3OGE3MGRjNzYxODllNTQ5ZGE4ODBjZmYzMzNjMTk0NTAyY2FkNWYwM2E3ZTExMTc5YTM="""\
"""VXNlcjQ="""\
"""Njc4OTEw"""

k = """U3VjY2VzcyEgWW91ciBrZXkgaXM6IDY3ODkxMA=="""\
"""MDIyMDJlNDgxZWE4MjdhMTA3NzNlZDFhN2MxY2FmNjk1MmJmNmU3OTViOWIwOTFjNGYwODc1YzRlZmU3MTUzOWI1ODU3ZTkwMzAzOWJkMjk3MjE5NWNkZjNmYjJkOGVlYWM2MzkyYTk5ODA1Y2E2OTkzNmM5MWI1NTA3MTgxNTg="""\
"""bmljZSB0cnkgYnV0IHBsZWFzZSBkb250IGp1c3QgZGVjb2RlIHRoaXM="""

u = """VXNlcjQ="""\
"""cHl0aG9uIHN0YXJ0ZXJfY29kZV8xLnB5"""\
"""eW91IGNvdWxkIGhhdmUganVzdCBhbnN3ZXJlZCB0aGUgcXVlc3Rpb24gaW4gdGhlIHRpbWUgaXQgdG9vayB5b3UgdG8gZGVjb2RlIGFsbCB0aGlz"""

def unpackt(func):
	def wrapper(*args, **kwargs):
		return str((base64.b64decode(t)))
	return wrapper

def unpackk(func):
	def wrapper(*args, **kwargs):
		return str((base64.b64decode(k)))
	return wrapper

def unpacku(func):
	def wrapper(*args, **kwargs):
		return str((base64.b64decode(u)))
	return wrapper

@unpackt
def get_block_1():
	return t[0:128] 

@unpackt
def get_block_2():
	return t[128:256] 

@unpackt
def get_block_3():
	return t[256:368] 

@unpackk
def get_block_4():
	print t
	return t[0:128]

@unpackk
def get_block_5():
	return t[128:256] 

@unpackk
def get_block_6():
	return t[256:368] 

@unpacku
def get_block_7():
	return t[0:128]

@unpacku
def get_block_8():
	return t[128:256] 

@unpacku
def get_block_9():
	return t[256:368]  