#!/usr/bin/env python

import socket

http_string = "GET / HTTP/1.1\n\n"
portpair = ('127.0.0.1', 5000) # webserver is running on port 5000 of the grader

"""
 The objective of this task is to utilize a relatively simple REST API provided by
 a webserver running on port 5000 of the grader system to obtain a key.
 
-> The /user/<user_id>/ url accepts a fictional HTTP "KEY" method, which will give you a value
 that will need to be sent to /key/<user_id>/<key>/ URL via an HTTP "PUT" method.

 Ask DOV for your user_id
"""
# !/usr/bin/env python

import socket
import httplib
  # webserver is running on port 5000 of the grader

g_user_id = "User3"

"""
 The objective of this task is to utilize a relatively simple REST API provided by
 a webserver running on port 5000 of the grader system to obtain a key.

-> The /user/<user_id>/ url accepts a fictional HTTP "KEY" method, which will give you a value
 that will need to be sent to /key/<user_id>/<key>/ URL via an HTTP "PUT" method.

 Ask DOV for your user_id
"""


def make_request(method, url):
	conn = httplib.HTTPConnection(portpair[0], portpair[1])
	print("Sending method %s to %s" % (method, url))
	conn.request(method, url, "")
	return conn.getresponse()


if __name__ == '__main__':
	resp = make_request("KEY", "/user/{}/".format(g_user_id))

	print("Response {} {}:".format(resp.status, resp.reason))

	key_val = ""

	if resp.status == 200:
		print("Body:\n")
		key_val = resp.read()
		print(key_val + "\n")

	resp = make_request("PUT", "/key/{}/{}/".format(g_user_id, key_val))

	print("Response {} {}:".format(resp.status, resp.reason))

	if resp.status == 200:
		print("Body:\n")
		key = resp.read()
		print(key + "\n")


