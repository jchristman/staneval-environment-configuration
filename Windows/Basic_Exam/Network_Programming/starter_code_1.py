#!/usr/bin/env python

import base64
import httplib, socket

http_string = "GET / HTTP/1.1\r\n\r\n"
portpair = ('192.168.10.100', 5000) # webserver is running on port 5000 of the grader

g_user_id = ""

"""
 The objective of this task is to utilize a relatively simple REST API provided by
 a webserver running on port 5000 of the grader system to obtain a key.

-> Create a function called make_request that takes in a HTTP method and a URL.

-> You will need to call this method twice (make two requests).
   The first request: /user/<user_id>/ url accepts a fictional HTTP "KEY" method, which will return a key value.
   The second request: you will send the key value you recieved via an HTTP "PUT" method, /key/<user_id>/<key_val>/.

  Ask your proctor for your user_id
"""

def make_request(method, url):
	
	pass


