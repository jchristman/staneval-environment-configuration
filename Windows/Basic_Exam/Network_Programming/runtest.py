import unittest, xmlrunner
from starter_code_1 import *
from helper_stuff import *
from flask import Flask

class TestFlaskServer(unittest.TestCase): 

  def test_correct_user(self):
    resp = make_request("KEY", "/user/{}/".format(get_block_7()))
    self.assertEqual(200, resp.status) 

  # test method
  def test_get_key(self):
    resp = make_request("KEY", "/user/{}/".format(get_block_7()))
    self.assertEqual(200, resp.status) 
    self.assertEqual(str(get_block_1()), resp.read())

  def test_key_value(self):
    resp = make_request("PUT", "/key/{}/{}/".format(get_block_7(), get_block_1()))
    self.assertEqual(resp.read(), get_block_4())     

# runs the unit tests in the module
if __name__ == '__main__':
  #unittest.main()
  with open('unittest.xml', 'w') as output:
    unittest.main(
    testRunner=xmlrunner.XMLTestRunner(output=output), 
    failfast=False, 
    buffer=False, 
    catchbreak=False
  )