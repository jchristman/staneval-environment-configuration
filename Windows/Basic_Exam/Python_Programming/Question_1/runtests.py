#!/usr/bin/env python

import unittest, subprocess, os, re, base64, xmlrunner
from testfile import *
from helper_stuff import *

class CheckRideTestOne(unittest.TestCase):
	def test_string_reversal(self):
		exp_input = [
		"abcdefg",
		"hijklmn",
		"opqrstu",
		"vwxyz12",
		]

		expected_output = "gfedcba nmlkjih utsrqpo 21zyxwv"
		self.assertEqual(expected_output, first_test(exp_input))


if __name__ == '__main__':
    #unittest.main()
    with open('unittest.xml', 'w') as output:
      unittest.main(
      testRunner=xmlrunner.XMLTestRunner(output=output), 
      failfast=False, 
      buffer=False, 
      catchbreak=False
      )
