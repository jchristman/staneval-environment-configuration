#!/usr/bin/env python

import unittest, subprocess, os, re, base64, xmlrunner
from testfile import *
from helper_stuff import *

class CheckRideTestOne(unittest.TestCase):

	def test_get_text_files(self):

		cleanup("evalFolder")

		get_block_4() # initialize test
		tmp = get_block_5()
		output = third_test("evalFolder")
		for k in tmp.keys():
			self.assertEqual(tmp.get(k), output.get(k))

		cleanup("evalFolder")


if __name__ == '__main__':
	#unittest.main()
    with open('unittest.xml', 'w') as output:
      unittest.main(
      testRunner=xmlrunner.XMLTestRunner(output=output), 
      failfast=False, 
      buffer=False, 
      catchbreak=False
      )

