﻿$msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\MSBuild.exe"
$examFolder = "C:\\Users\\vagrant\\Desktop\\Exam"
$vagrantFolder = "C:\\vagrant"
$python = "python"
$testnum = 0
$options = " /p:Configuration=Debug" 
$msBuildExe = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe'

#$Eval_folders = get-childitem -path $examFolder | where-object {$_.psiscontainer -eq "True"} | select-object name 

  #Perform test of C Programming section of an evaluation
  $cprogramming = $examFolder + "\\C_Programming\\" #build folder path
  $checkpath = Test-Path $cprogramming #verify folder exists
  if($checkpath -eq $True)
  {
    Set-Location $cprogramming 
    $pquestion_folders = get-childitem -path $cprogramming | where-object {$_.psiscontainer -eq "True"} | select-object name 
    foreach($pquestion_folder in $pquestion_folders) #iterate through each Python question folder
    {
        $cquestion = $cprogramming + $pquestion_folder.Name
        Set-Location $cquestion
        & "$($msBuildExe)" ".\CProgramming.sln" /t:Build /m
        #$result = iex "& .\\Debug\\CProgramming.exe" 
        iex "& .\\Debug\\*.exe"
         
    }
   }
  else #if folder does not exist exit 
  {
    Exit 1
  }

  $assembly = $examFolder + "\\Assembly_Programming\\" #build assembly path yada yada
  $checkpath = Test-Path $assembly 
  if($checkpath -eq $True)   #if assembly folder exists 
  {
    Set-Location $assembly
    & "$($msBuildExe)" ".\ASM.sln" /t:Build /m
    iex "& .\\Debug\\ASM.exe"
  }
  else  #if assembly folder does not exist 
  {
    Exit 1
  }

  $pythonfolder = $examFolder + "\\Python_Programming\\"
  $checkpath = Test-Path $pythonfolder
  if($checkpath -eq $True)   
  {
    Set-Location $pythonfolder
    $pquestion_folders = get-childitem -path $pythonfolder | where-object {$_.psiscontainer -eq "True"} | select-object name 
    foreach($pquestion_folder in $pquestion_folders) #iterate through each Python question folder
    {
        $pquestion = $pythonfolder + $pquestion_folder.Name
        Set-Location $pquestion

        $prog = Get-ChildItem 'r*.py'
        $runtests = "runtests.py"
        $result = iex "& $python $runtests" 
    }
  }
  else
  {
    Exit 1
  }

  #Perform validation of Network Programming question 
  $networkprog = $examFolder + "\\Network_Programming\\"
  $checkpath = Test-Path $networkprog
  if($checkpath -eq $True)
  {
    Set-Location $networkprog
    $prog = Get-ChildItem -Path $networkprog -File -Filter "*.py"
    $result = iex "& $python $prog"
  }

  else
  {
    Exit 1
  }
