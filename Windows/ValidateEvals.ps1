﻿$msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"
#$repository = "C:\\Users\DOV\\Documents\\CICD_Services\\evaluation-sets\\"
$repository = "C:\\Users\vagrant\\Desktop\\exam\\"
$testnum = 0

$options = " /p:Configuration=Debug" 
Set-Location $repository
if(Test-Path ".\validate_eval_repo.txt")
{
    Remove-Item ".\validate_eval_repo.txt"   
}


#Perform test of C Programming section of an evaluation
$cprogramming = $repository + $eval_folder.Name + "\\C Programming\\" #build folder path
$checkpath = Test-Path $cprogramming #verify folder exists
if($checkpath -eq $True)
{
    Set-Location $cprogramming 
    Write-Output "Cleaning $cprogramming solution..." | Add-Content validate_eval_repo.txt 
    $clean = "CProgramming.sln", "/t:Clean",  "/p:Configuration=Debug", "/p:Platform=Win32"
    Write-Output $msbuild $clean | Add-Content validate_eval_repo.txt 
    $result = &$msbuild $clean #clean solution 
    Write-Output $result| Add-Content validate_eval_repo.txt 
    Write-Output "Attempting build of $cprogramming solution ..." | Add-Content validate_eval_repo.txt 
    $build = "CProgramming.sln","/t:Build", "/p:Configuration=Debug", "/p:Platform=Win32"   #solution properties must be set! Error from MSbuild otherwise
    $result = &$msbuild $build #build solution

    if($LastExitCode -ne 0) #if error encountered while building solution
    {
       Write-Output ("Error building $cprogramming solution...") | Add-Content validate_eval_repo.txt 
       Write-Output $result| Add-Content validate_eval_repo.txt 
       Write-Output ("Validation failed for $cprogramming ...") | Add-Content validate_eval_repo.txt 
       Exit 1 
    }
    Write-Output $result
}

else #if folder does not exist exit 
{
    Write-Output ("Error: $cprogramming is not found...") | Add-Content validate_eval_repo.txt 
    Exit 1
}


#Perform test of Assembly Programming section of an evaluation
$assembly = $repository + $eval_folder.Name + "\\Assembly Programming\\" #build assembly path yada yada
$checkpath = Test-Path $assembly 
if($checkpath -eq $True)   #if assembly folder exists 
{
    Set-Location $assembly
    Write-Output ("Cleaning $assembly solution...") | Add-Content validate_eval_repo.txt 
    $clean = "ASM.sln", "t:Clean", "/p:Configuration=Debug", "/p:Platform=Win32"
    $result = &$msbuild $clean #clean solution 
    Write-Output $result| Add-Content validate_eval_repo.txt 
    Write-Output ("Attempting build of $assembly solution ..." )| Add-Content validate_eval_repo.txt 
    $build = "ASM.sln", "/t:Build", "/p:Configuration=Debug", "/p:Platform=Win32"   #set build properties 
    $result = &$msbuild $build     #build solution

    if($LastExitCode -ne 0)  #if error during build 
    {
        Write-Output ("Error building $assembly solution...") | Add-Content validate_eval_repo.txt 
        Write-Output $result| Add-Content validate_eval_repo.txt 
        Write-Output ("Validation failed for $assembly ...") | Add-Content validate_eval_repo.txt 
        Exit 1
    }
    Write-Output $result| Add-Content validate_eval_repo.txt 
 }

else  #if assembly folder does not exist 
{
    Write-Output ("Error: $assembly is not found...") | Add-Content validate_eval_repo.txt 
    Exit 1
}
  
#Perform validation of Python Programming questions 
$python = $repository + $eval_folder.Name + "\\Python Programming\\"
$checkpath = Test-Path $python
if($checkpath -eq $True)   
{
    Set-Location $python
    $pquestion_folders = get-childitem -path $python | where-object {$_.psiscontainer -eq "True"} | select-object name 
    foreach($pquestion_folder in $pquestion_folders) #iterate through each Python question folder
    {
        $pquestion = $python + $pquestion_folder.Name
        Set-Location $pquestion
        $check_runtests = Test-Path "runtests.py" -PathType Leaf #verify runtests source code file exists 
        if($check_runtests -eq $False)
        {
            Write-Output ("Error: runtests.py not found in $pquestion ...")| Add-Content validate_eval_repo.txt 
            Exit 1
        }

        $check_testfile = Test-Path "testfile.py" -PathType Leaf #verify testfile source code exists
        if($check_testfile -eq $False)
        {
            Write-Output ("Error: testfile.py not found in $pquestion ...") | Add-Content validate_eval_repo.txt 
            Exit 1
        }
    }
}

else
{
    Write-Output ("Error: $python not found ...") | Add-Content validate_eval_repo.txt 
    Exit 1
}

#Perform validation of Network Programming question 
$networkprog = $repository + $eval_folder.Name + "\\Network Programming\\"
$checkpath = Test-Path $networkprog
if($checkpath -eq $True)
{
    Set-Location $networkprog
    Write-Output ("Checking for starter_code.py in $networkprog")| Add-Content validate_eval_repo.txt 
    $check_starter_code = Test-Path "starter_code_*.py" -PathType Leaf  #verify that a python file that starts with starter_code exists 
    if($check_starter_code -eq $False)
    {
        Write-Output ("Error: starter_code.py not found in $networkprog ...") | Add-Content validate_eval_repo.txt 
        Exit 1
    }
    Write-Output ("starter_code.py found in $networkprog ") | Add-Content validate_eval_repo.txt 

}

else
{
    Write-Output ("Error: $networkprog not found ...") | Add-Content validate_eval_repo.txt 
    Exit 1
}

Write-Output ("Finished validating evaluation sets") | Add-Content validate_eval_repo.txt 
return 0
 
  
