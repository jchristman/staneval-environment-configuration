rem There is a race between BoxStarter reboots and Vagrant sync'ed folders, so copy the gist to a temp file
copy "%systemdrive%\vagrant\shell\box_starter_gist.txt" "%temp%\box_starter_gist.txt"
@powershell -NoProfile -ExecutionPolicy Bypass -Command "Install-BoxStarterPackage -PackageName %temp%\\box_starter_gist.txt"  
