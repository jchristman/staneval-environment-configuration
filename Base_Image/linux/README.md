This folder and its Vagrantfile is inteded to create a base box for evaluations.
A base box will be a starting point to create the full virtualized environment used for examinations.
Base boxes are created to limit the provisioning time neccessary before exams.

Provisioning steps
  1.  Provision the base VM
    > vagrant up
  2.  Package the VM
    > vagrant package --output linux_eval_<version>.box
  3.  Edit the file `linux_base.json` to add the new version
  4.  Upload the new JSON and box to the server
    > curl -T linux_base.json http://<hostname>:3000/static
    > curl -T linux_eval_<version>.box http://<hostname>:3000/static